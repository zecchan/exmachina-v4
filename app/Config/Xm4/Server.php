<?php

namespace Config\Xm4;

use CodeIgniter\Config\BaseConfig;

/**
 * Stores the settings for ExMachina v4
 */
class Server extends BaseConfig
{
	/**
	 * Name of the server
	 *
	 * @var string
	 */
	public $name = 'ExMachina';

	/**
	 * Domain name of the server
	 *
	 * @var string
	 */
	public $domainName = '';

	/**
	 * Expiration of JWT token and cookie
	 *
	 * @var int
	 */
	public $jwtExpiration = 604800;

	/**
	 * Is server under maintenance?
	 *
	 * @var bool
	 */
    public $maintenance = false;

	/**
	 * Elastic key that is used for generating nonces such as password reset link.
	 * Change this when needed. It will invalidate all single-use links and codes.
	 */
	public $nonceKey = '';

	/**
	 * Secret key that is used to generate tokens. Changing this will invalidate all tokens.
	 * Change this if you got massive session hijacking attack. (jk)
	 */
	public $secretKey = '';

	/**
	 * JWT token expiration time.
	 */
	public $jwtTokenExpiration = 86400;

	/**
	 * Allows JWT tokens to be refreshed.
	 */
	public $jwtTokenAllowRefresh = true;

	/**
	 * DO NOT EDIT
	 * Static key that is used for passwords and static data. Changing this will render all passwords unusable and all static data inaccessible.
	 */
	public $staticKey = '';

	/**
	 * DO NOT EDIT
	 * When the values of this config is loaded from Server.json, the value is true
	 */
	public $fromJson = false;

	/**
	 * DO NOT EDIT
	 * When the engine is installed, this will be true
	 */
	public $installed = false;

	/**
	 * DO NOT EDIT
	 * Authorization method(s) used
	 */
	public $auth = [];

	/**
	 * Used to load configuration from Server.json
	 */
	public function __construct() {
		if (\file_exists(APPPATH . "Config/Xm4/Server.json")) {
			$o = json_decode(file_get_contents(APPPATH . "Config/Xm4/Server.json"), true);
			if (isset($o))
				foreach($o as $k => $v) {
					if (isset($this->$k))
						$this->$k = $v;
				}
			$this->fromJson = true;
		}
		$this->installed = \file_exists(APPPATH . "Config/Xm4/Install.json");
	}
}
