<?php

// override core en language system validation or define your own en language validation message
return [
    "identifier" => "'{value}' is not a valid identifier",
    "name" => "'{name}' is not a valid identifier"
];
