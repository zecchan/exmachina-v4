<?php
namespace App\Controllers;

use Xm4\BaseController;

class Home extends BaseController
{
	public function index()
	{
		return view('welcome_message');
	}
}
