<?php
namespace App\Controllers\Api\Coatl\Xm4\v1;

use Exception;
use Xm4\APIController;

class Licensing extends APIController
{
    protected $alwaysAvailable = true;

	public function index()
	{
        $this->access
            ->method(["POST"])
            ->check();
        
        $json = $this->apirequest->validateJSON([
            "appId" => "required",
            "licenseCode" => "required",
        ]);
        
        if ($this->user->isAuthenticated()) {

        } else {

        }
        
        return $this->api->json()->data(200);
	}

    public function checkSerial() {
        $this->access
            ->method(['POST'])
            ->check();
        
        $json = $this->apirequest->validateJSON([
            "serial" => "required"
        ]);
        try {
            $mdl = new \Xm4\Models\LicenseModel();
            $serial = ($json->serial ?? "") . "";
            if (!$mdl->checkSerial($serial))
                return $this->api->message(400, "Serial number is not valid");
            $pureSerial = strtolower(str_replace("-", "", $serial));
            $re = $mdl->where('serial', $pureSerial)->first();
            if (!isset($re))
                return $this->api->message(404, "Serial number is not registered");
            $d = [
                "serialNumber" => $mdl->formatSerial($pureSerial),
                "applicationId" => $re['appid'],
                "activationStatus" => $re['active'] ? "Activated" : "Not Activated",
            ];
            if ($re['active']) {
                if (isset($re['activatedon']))
                    $d['activatedOn'] = date('Y-m-d H:i:s', strtotime($re['activatedon']));
                $d['activeUntil'] = isset($re['activeuntil']) ? date('Y-m-d H:i:s', strtotime($re['activeuntil'])) : "Permanent";
                $d['licenseKey'] = $re['license'];
                $lpss = json_decode($re['licensepricingsnapshot']);
                if (isset($lpss) && isset($lpss->hwbound) && $lpss->hwbound) {
                    $d['hardwareChangeQuotaLeft'] = intval($re['hwchangeleft']);
                }
                if (isset($re['userid'])) {
                    $mdlu = new \Xm4\Models\UserModel();
                    $u = $mdlu->find($re['userid']);
                    $d['licensedTo'] = trim(str_replace('  ', ' ', $u->givenname . " " . $u->middlename . " " . $u->surname));
                }
            }
            return $this->api->data(200, "Serial number is valid and registered", $d);
        }
        catch(Exception $ex) {
            return $this->api->message(500, $ex->getMessage());
        }
    }

    public function generateSerial() {
        $this->access
            ->method(['POST'])
            ->authorize(true)
            ->check();
        
        $json = $this->apirequest->validateJSON([
            "licensepricingid" => "required"
        ]);
        try {
            $count = intval($json->count ?? "1");
            if ($count < 1) $count = 1;
            $mdl = new \Xm4\Models\LicenseModel();
            $re = $mdl->generateSerial(intval($json->licensepricingid), $count);
            if (is_array($re))
                return $this->api->data(200, "Serial generated successfully", $re);
            return $this->api->error(400, "Failed to generate serial, pricing id is not found");
        }
        catch(Exception $ex) {
            return $this->api->error(500, $ex->getMessage());
        }
    }

    public function activate() {
        $this->access
            ->method(['POST'])
            ->check();
        
        $json = $this->apirequest->validateJSON([
            "serial" => "required"
        ]);
        try {
            $mdl = new \Xm4\Models\LicenseModel();
            $serial = ($json->serial ?? "") . "";
            if (!$mdl->checkSerial($serial))
                return $this->api->message(400, "Serial number is not valid");
            $pureSerial = strtolower(str_replace("-", "", $serial));
            $re = $mdl->where('serial', $pureSerial)->first();
            if (!isset($re))
                return $this->api->message(404, "Serial number is not registered");
            
            // serial is valid
            $uid = null;
            if ($this->user->isAuthenticated()) {
                $uid = $this->user->getId();
            }
            if (isset($re['userid']) && $re['userid'] != $uid) {
                return $this->api->message(404, "Serial number is owned by someone else");
            }

            $data = [
                "userid" => $uid
            ];

            $hwi = null;
            $hwkey = null;
            $hwq = intval($re['hwchangeleft']);
            $lpss = json_decode($re['licensepricingsnapshot']);
            if (!isset($lpss))
                return $this->api->message(400, "License is not valid");
            if (isset($lpss) && isset($lpss->hwbound) && $lpss->hwbound) {
                if (!isset($json->hwidentifier) || !is_string($json->hwidentifier) || trim($json->hwidentifier) == "") 
                    return $this->api->message(400, "This serial number needs Hardware Identifier");
                $hwi = trim($json->hwidentifier);
                $hwkey = $hwi ? substr(hash('sha256', $hwi), 0, 32) : null;
                if (strlen($hwi) < 32)
                    return $this->api->message(400, "Hardware Identifier must be at least 32 characters");
                if ($hwq < 1 && $hwkey != $re['hwkey'])
                    return $this->api->message(400, "This serial number can no longer be activated in different hardware");
                if ($hwkey != $re['hwkey'])
                    $hwq--;
                $data["hwkey"] = $hwkey;
                $data["hwchangeleft"] = $hwq;
            }

            if (!isset($re['activeuntil']))
            {
                if ($lpss->permanent == 1) {
                    $data['activeuntil'] = null;
                }
                else if ($lpss->demo == 1) {

                }
                else if ($lpss->credit == 1) {

                }
                else if ($lpss->subscription == 1) {

                }
            }

            $data['license'] = $mdl->buildLicense($pureSerial, $re['appid'], $hwkey);
            $data['activatedon'] = date('Y-m-d H:i:s');

            $res = [
                "serialNumber" => $mdl->formatSerial($pureSerial),
                "licenseKey" => $mdl->formatSerial($data['license']),
            ];
            //if (isset($data["hwkey"])) $res["hardwareKey"] = $mdl->formatSerial($data['hwkey']);
            if (isset($data['activeuntil']))
                $res['activeUntil'] = $data['activeuntil'];
            else
                $res['activeUntil'] = 'Permanent';

            if ($mdl->update($re['id'],$data))
                return $this->api->data(200, "Activated successfully", $res);
            return $this->api->message(500, "Failed to activate serial");
        }
        catch(Exception $ex) {
            return $this->api->message(500, "Something is wrong, please try again later");
        }
    }
}
