<?php
namespace App\Controllers\Api\Coatl\Xm4\v1;

use Xm4\APIController;

class Organization extends APIController
{
    public function create() {
        $this->access
            ->method(["POST"])
            ->authorize()
            ->check();

		$model = new \Xm4\Models\OrganizationModel();
        $json = $this->apirequest->validateJSON($model->getValidationRules([
            "except" => ["banned", "active"]
        ]), true);
        if (isset($json["id"])) unset($json["id"]);
        $json["active"] = 1;
        $json["banned"] = 0;
        $json["creatorid"] = $this->user->getId();
		if (!$model->insert($json))
		{
			$err = $model->errors();
			return $this->api->json()->error(400, "Organization creation failed", $err);
		}

        $oid = $model->insertID;
        $mdlm = new \Xm4\Models\OrganizationMemberModel();
        $data = [
            "userid" => $this->user->getId(),
            "organizationid" => $oid,
            "active" => 1,
            "banned" => 0,
            "joinedat" => date("Y-m-d H:i:s"),
            "bannedat" => null,
        ];
        if (!$mdlm->insert($data)) {
            $model->where('id', $oid)->delete();
			$err = $mdlm->errors();
			return $this->api->json()->error(400, "Organization creation failed", $err);
        }

        return $this->api->json()->message(200, "Organization creation success");
    }

	public function invite()
	{
        $this->access
            ->method(["POST"])
            ->authorize()
            ->check();

		$model = new \Xm4\Models\OrganizationInviteModel();
        $json = $this->apirequest->validateJSON([
            "userid" => "required|integer",
            "organizationid" => "required|integer"
        ], false);
        $res = $model->makeInvite($this->user->getId(), $json->userid, $json->organizationid);
        if ($res === true)
            return $this->api->message(200, "Invitation success");
        return $this->api->error(400, "Invitation failed", $res);
	}

	public function answerInvite()
	{
        $this->access
            ->method(["POST"])
            ->authorize()
            ->check();

		$model = new \Xm4\Models\OrganizationInviteModel();
        $json = $this->apirequest->validateJSON([
            "invitationid" => "required|integer"
        ], false);
        if (!isset($json->accept)) $json->accept = true;
        $res = $model->acceptInvite($json->invitationid, $this->user->getId(), $json->accept !== false);
        if ($res === true)
            return $this->api->message(200, "Invitation response success");
        return $this->api->message(400, $res);
	}
}
