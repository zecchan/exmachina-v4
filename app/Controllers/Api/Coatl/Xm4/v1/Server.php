<?php
namespace App\Controllers\Api\Coatl\Xm4\v1;

use Xm4\APIController;

class Server extends APIController
{
    protected $alwaysAvailable = true;

	public function index()
	{
		$o = [
            "name"          => $this->exm4Config->name,
            "engine"        => "ExMachina",
            "engineVersion" => \Xm4\ExMachina::getVersion(),
            "maintenance"   => $this->exm4Config->maintenance,
        ];
        return $this->api->json()->data(200, "", $o);
	}
}
