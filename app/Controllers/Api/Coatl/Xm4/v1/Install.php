<?php

namespace App\Controllers\Api\Coatl\Xm4\v1;

use CodeIgniter\Events\Events;
use Xm4\APIController;

class Install extends APIController
{
    protected $alwaysAvailable = true;
    
    public function index()
    {
        if ($this->exm4Config->installed)
            return $this->api->message(403, "Forbidden");
        $json = $this->apirequest->getJSON();
        if (!isset($json))
            return $this->api->message(400, "Install payload is missing");
        
        $log = [];
        $migrate = \Config\Services::migrations();
        $migrate->setNamespace(null);
        try {
            $res = $migrate->latest();
            if (!$res)
                return $this->api->json()->data(500, "Failed to migrate database", $migrate->getError());
            $log[] = "Migration completed successfully";
        } catch (\Throwable $e) {
            return $this->api->json()->message(500, $e->getMessage());
        }

        $data = [
            "config" => $json,
            "log" => $log
        ];
        
        return $this->api->json()->data(200, "Installed successfully", $data);
    }

    public function migrateLatest()
    {
        $this->access
            ->authorize(true)
            ->method(["POST", "options"])
            ->origin(".")
            ->check();
        $migrate = \Config\Services::migrations();
        $migrate->setNamespace(null);
        try {
            $res = $migrate->latest();
            return $this->api->json()->message($res ? 200 : 500, $res ? "Database migrated successfully" : "Failed to migrate database");
        } catch (\Throwable $e) {
            return $this->api->json()->message(500, $e->getMessage());
        }
    }

    public function migrateRegress($batch = -1)
    {
        $this->access
            ->authorize(true)
            ->method(["POST", "options"])
            ->origin(".")
            ->check();
        if (!is_numeric($batch)) return $this->api->message(400, "Batch count is invalid");
        $migrate = \Config\Services::migrations();
        $migrate->setNamespace(null);
        try {
            $res = $migrate->regress($batch);
            return $this->api->json()->message($res ? 200 : 500, $res ? "Database regressed successfully" : "Failed to regress database or database is already regressed to 0");
        } catch (\Throwable $e) {
            return $this->api->json()->message(500, $e->getMessage());
        }
    }

    public function uninstallxm4a(){
        $this->access
            ->authorize(true)
            ->method(["POST", "options"])
            ->origin(".")
            ->check();
        $json = $this->apirequest->validateJSON([
            "appid" => "required"
        ]);
        $ins = new \Xm4\Xm4a\Xm4aInstaller();
        $log = $ins->uninstall($json->appid);
        return $this->api->data($log->success ? 200 : 500, null, $log);
    }

    public function testInstallPackage(){
        $path = 'M:\WebServers\x2021_02\htdocs\exm4\xm4a\example\myapp.xm4a';
        $ins = new \Xm4\Xm4a\Xm4aInstaller();
        $log = $ins->install($path);
        return $this->api->data($log->success ? 200 : 500, null, $log);
    }
}
