<?php
namespace App\Controllers\Api\Coatl\Xm4\v1;

use CodeIgniter\Config\Config;
use Xm4\APIController;
use Xm4\Models\UserModel;

class Auth extends APIController
{
    protected $alwaysAvailable = true;

	public function maketoken()
	{
        $this->access
            ->method(["POST"])
            ->check();

		$model = new \Xm4\Models\UserModel();
        $json = $this->apirequest->validateJSON([
            "username" => 'required',
            'password' => 'required'
        ]);

        $usr = $model->where('username', $json->username)->orWhere('email', $json->username)->first();
        if (
            !isset($usr)
            || !\Xm4\Models\UserModel::comparePassword($json->password, $usr->password, $usr->secret)
        ) return $this->api->error(400, "Authentication failed");

        $jwt = new \Xm4\Auth\JWTService();
        $token = $jwt->makeUserToken($usr, 0);
        if (isset($json->setCookie) && $json->setCookie === true) {
            $this->response->setCookie('token', $token, $this->exm4Config->jwtExpiration);
        }
        $resp = [
            "token" => $token
        ];
        return $this->api->json()->data(200, "", $resp);
	}

    /** Only used for web logins, does not affect bearer or basic */
    public function logout() {
        $this->access
            ->method(["GET", "options"])
            ->authorize()
            ->check();
        $this->response->deleteCookie('token');
        return $this->api->message(200, "Logged out");
    }

	public function index()
	{
        $this->access
            ->method(["GET", "options"])
            ->check();
        return $this->api->json()->data(200, $this->user->isAuthenticated() ? "Authenticated" : "Not Authenticated", $this->user->getClaims());
	}
}
