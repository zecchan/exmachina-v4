<?php
namespace App\Controllers\Api\Coatl\Xm4\v1;

use Xm4\APIController;

class User extends APIController
{
	public function register()
	{
        $this->access
            ->method(["POST"])
            ->check();

		$model = new \Xm4\Models\UserModel();
        $json = $this->apirequest->validateJSON($model->getValidationRules(), true);
		unset($json['superuser']);
		$json['secret'] = \Xm4\Helper::randomString(64);
		if (!$model->save($json))
		{
			$err = $model->getError();
			return $this->api->json()->error(400, "Registration failed", $err);
		}
        return $this->api->json()->message(200, "Registration success");
	}
}
