<?php
namespace Xm4;

class JSONConfigBase {
    /**
     * Used to specify which configuration should be used
     */
    protected $configFileName = "Config.json";

	/**
	 * Used to load configuration from configuration file
	 */
	public function __construct() {
		if (\file_exists(APPPATH . "Config/" . $this->configFileName )) {
			$o = json_decode(file_get_contents(APPPATH . "Config/" . $this->configFileName), true);
			if (isset($o))
				foreach($o as $k => $v) {
					if (isset($this->$k))
						$this->$k = $v;
				}
		}
	}
}