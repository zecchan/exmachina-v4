<?php
namespace Xm4;

use CodeIgniter\Controller;
use CodeIgniter\Events\Events;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

use Xm4\Http\Results;
use Xm4\Http\Requests;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */

class BaseController extends Controller
{
	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = [];

	/**
	 * 
	 * @var \Config\Xm4\Server
	 */
	protected $exm4Config = null;

	/**
	 * Gets the api result.
	 * 
	 * @var \Xm4\Http\Results\ApiResult
	 */
	protected $api = null;

	/**
	 * Gets the api request.
	 * 
	 * @var \Xm4\Http\Requests\ApiRequest
	 */
	protected $apirequest = null;

    protected $alwaysAvailable = false;

	protected $apiController = false;

	/**
	 * Gets the current user.
	 * 
	 * @var \Xm4\Auth\ClaimsPrincipal
	 */
	protected $user = null;

	/**
	 * Gets the access level processor.
	 * 
	 * @var \Xm4\Auth\AccessLevel
	 */
	protected $access = null;

	public static $authPrincipal = null;

	/**
	 * Constructor.
	 *
	 * @param RequestInterface  $request
	 * @param ResponseInterface $response
	 * @param LoggerInterface   $logger
	 */
	public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);
		
		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.: $this->session = \Config\Services::session();
		$this->exm4Config = new \Config\Xm4\Server();
		$this->api = new Results\ApiResult($response);
		$this->apirequest = new Requests\ApiRequest($request, $this->api);
		$this->user = BaseController::$authPrincipal ?? new \Xm4\Auth\ClaimsPrincipal();
		if (count($this->user->identities) == 0)
			$this->user->addUnauthorizedIdentity();

		if (!$this->alwaysAvailable) {
			if ($this->exm4Config->maintenance)
			{
				$this->api->message(503, "Server is under maintenance");
				$response->send();
				die();
			}
		}

		$this->access = new \Xm4\Auth\AccessLevel($request, $response, $this->user);
		$this->access->isAPIController = $this->apiController;
	}
}
