<?php
namespace Xm4\Http\Requests;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Xm4\Http\Results\ApiResult;

/**
 * Class ApiRequest
 *
 * ApiRequest provides a convenient way to parse an API request.
 * 
 */

class ApiRequest {
    /**
     * 
     * 
     * @var RequestInterface
     */
    protected $request;
    /**
     * 
     * 
     * @var ApiResult
     */
    protected $result;

    public function __construct(RequestInterface $request, ApiResult $result) {
        $this->request = $request;
        $this->result = $result;
    }

    public function getJSON(bool $asArray = false, bool $dieIfFail = true) {
        $json = $this->request->getJSON($asArray);
        if (!isset($json)) {
            if ($dieIfFail) {
                $this->result->error(400, "Payload is not present")->send();
                die;
            }
            return null;
        }
        return $json;
    }

    public function validateJSON(array $validationRules, bool $asArray = false, bool $dieIfFail = true) {
        $json = $this->getJSON(true, $dieIfFail);
        $validation =  \Config\Services::validation();
        $validation->setRules($validationRules);
        if (!$validation->run($json)) {
            if ($dieIfFail) {
                $errs = $validation->getErrors();
                $this->result->error(400, "Input validation failed", $errs)->send();
                die();
            }
            return false;
        }
        if (!$asArray) {
            $json = json_decode(json_encode($json));
        }
        return $json;
    }
}

