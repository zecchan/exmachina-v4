<?php
namespace Xm4\Http\Results;

use CodeIgniter\HTTP\ResponseInterface;

/**
 * Class ApiResult
 *
 * ApiResult provides a convenient way to return an API response.
 * 
 */

class ApiResult {
    protected $response;
    protected $mode = "application/json";

    public function __construct(ResponseInterface $response) {
        $this->response = $response;
    }

    public function json() {
        $this->mode = "application/json";
        return $this;
    }

    public function xml() {
        $this->mode = "application/xml";
        return $this;
    }

    public function result($data, int $statusCode = 200) {
        $re = $this->response;

        $payload = $data;
        if ($this->mode == "application/json")
            $payload = \json_encode($data);
        if ($this->mode == "application/xml") {
            if (isset($data) && \is_object($data))
                $data = (array)$data;
            if (!is_array($data))
                $data = ["value" => $data];
            $xml = new \SimpleXMLElement("<?xml version=\"1.0\" encoding=\"utf-8\"?><root></root>");
            $this->array_to_xml($data, $xml);
            $payload = $xml->asXML();
        }

        $re
            ->setStatusCode($statusCode)
            ->setContentType($this->mode)
            ->setBody($payload);

        return $re;
    }

    protected function array_to_xml( $array, $xml = null ) {
        if ( is_array( $array ) ) {
            foreach( $array as $key => $value ) {
                if ( is_int( $key ) ) {
                    if ( $key == 0 ) {
                        $node = $xml;
                    } else {
                        $parent = $xml->xpath( ".." )[0];
                        $node = $parent->addChild( $xml->getName() );
                    }
                    } else {
                        $node = $xml->addChild( $key );
                    }
                $this->array_to_xml( $value, $node );
            }
        } else {
            $xml[0] = $array;
        }
    }
    
    public function message(int $statusCode, string $message = null) {
        $o = [
            "code" => $statusCode,
            "message" => $message
        ];
        return $this->result($o, $statusCode);
    }
    
    public function data(int $statusCode, string $message = null, $data = null) {
        $o = [
            "code" => $statusCode,
            "message" => $message,
            "data" => $data
        ];
        return $this->result($o, $statusCode);
    }

    public function error(int $statusCode, string $message = null, $errors = null) {
        $o = [
            "code" => $statusCode,
            "message" => $message,
            "errors" => $errors
        ];
        return $this->result($o, $statusCode);
    }
}

