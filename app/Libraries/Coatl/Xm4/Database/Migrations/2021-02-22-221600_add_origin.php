<?php

namespace Xm4\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddOrigin extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'          => [
                'type'       => 'VARCHAR',
                'constraint' => '32',
            ],
            'origins' => [
                'type'       => 'TEXT'
            ],
            'active' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 1
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->addKey('active');
        $this->forge->createTable('origins', FALSE, ['ENGINE' => 'InnoDB']);
    }

    public function down()
    {
        $this->forge->dropTable('origins');
    }
}
