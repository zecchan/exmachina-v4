<?php

namespace Xm4\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddLicense extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 30,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'userid'          => [
                'type'           => 'INT',
                'constraint'     => 30,
                'unsigned'       => true,
                'null'           => true,
            ],
            'organizationid'          => [
                'type'           => 'INT',
                'constraint'     => 30,
                'unsigned'       => true,
                'null'           => true,
            ],
            'appid' => [
                'type'       => 'VARCHAR',
                'constraint' => '64'
            ],
            'xm4appid' => [
                'type'       => 'VARCHAR',
                'constraint' => '64',
                'null'           => true,
            ],
            'serial' => [
                'type'       => 'VARCHAR',
                'constraint' => '20'
            ],
            'license' => [
                'type'       => 'VARCHAR',
                'constraint' => '32',
                'null'           => true,
            ],
            'hwkey' => [
                'type'       => 'VARCHAR',
                'constraint' => '32',
                'null'           => true,
            ],
            'hwchangeleft' => [
                'type'       => 'INT',
                'constraint' => 4,
                'default'    => 3
            ],
            'licensepricingid'          => [
                'type'           => 'INT',
                'constraint'     => 30,
                'unsigned'       => true,
                'null'           => true,
            ],
            'licensepricingsnapshot'          => [
                'type'           => 'TEXT',
                'null'           => true,
            ],
            // is it activated?
            'active' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 0
            ],
            'activatedon' => [
                'type'       => 'DATETIME',
                'null'       => true
            ],
            'activeuntil' => [
                'type'       => 'DATETIME',
                'null'       => true
            ],
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->addKey('active');
        $this->forge->addKey('appid');
        $this->forge->addKey('license');
        $this->forge->addUniqueKey('serial');
        $this->forge->addForeignKey('userid', 'users', 'id');
        $this->forge->addForeignKey('organizationid', 'organizations', 'id');
        $this->forge->addForeignKey('xm4appid', 'xm4apps', 'id');
        $this->forge->addForeignKey('licensepricingid', 'licensepricings', 'id');
        $this->forge->createTable('licenses', FALSE, ['ENGINE' => 'InnoDB']);
    }

    public function down()
    {
        $this->forge->dropTable('licenses');
    }
}
