<?php

namespace Xm4\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddUser extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 30,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'username' => [
                'type'       => 'VARCHAR',
                'constraint' => '50',
            ],
            'email' => [
                'type'       => 'VARCHAR',
                'constraint' => '120',
            ],
            'password' => [
                'type'       => 'VARCHAR',
                'constraint' => '64',
            ],
            'secret' => [
                'type'       => 'VARCHAR',
                'constraint' => '64',
            ],
            'givenname'       => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'middlename' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'surname' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'superuser' => [
                'type'       => 'INT',
                'constraint' => 1,
                'default'    => 0
            ],
            'active' => [
                'type'       => 'INT',
                'constraint' => 1,
                'default'    => 0
            ],
            'verified' => [
                'type'       => 'INT',
                'constraint' => 1,
                'default'    => 0
            ],
            'banned' => [
                'type'       => 'INT',
                'constraint' => 1,
                'default'    => 0
            ],
            'banreason' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'null'       => true
            ],
            'lastlogin' => [
                'type'       => 'DATETIME',
                'null'       => true
            ],
            'lastlogindata' => [
                'type'       => 'TEXT',
                'null'       => true
            ],
            'lastactivity' => [
                'type'       => 'DATETIME',
                'null'       => true
            ],
            'lastactivitydata' => [
                'type'       => 'TEXT',
                'null'       => true
            ],
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->addUniqueKey('username');
        $this->forge->addUniqueKey('email');
        $this->forge->addKey(['givenname', 'middlename', 'surname']);
        $this->forge->addKey(['lastlogin', 'lastactivity']);
        $this->forge->addKey(['active', 'verified', 'banned']);
        $this->forge->createTable('users', FALSE, ['ENGINE' => 'InnoDB']);
    }

    public function down()
    {
        $this->forge->dropTable('users');
    }
}
