<?php

namespace Xm4\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddOrganization extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 30,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'slug' => [
                'type'       => 'VARCHAR',
                'constraint' => '64'
            ],
            'name' => [
                'type'       => 'VARCHAR',
                'constraint' => '64'
            ],
            'address' => [
                'type'       => 'VARCHAR',
                'constraint' => '64',
                'null'           => true,
            ],
            'phone' => [
                'type'       => 'VARCHAR',
                'constraint' => '20',
                'null'           => true,
            ],
            'email' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'null'           => true,
            ],
            // is it activated?
            'active' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 0
            ],
            'banned' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 0
            ],
            'public' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 0
            ],
            'mustapprove' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 0
            ],
            'creatorid'          => [
                'type'           => 'INT',
                'constraint'     => 30,
                'unsigned'       => true,
                'null'           => true,
            ],
            'created_at' => [
                'type'       => 'DATETIME',
                'null'       => true
            ],
            'updated_at' => [
                'type'       => 'DATETIME',
                'null'       => true
            ],
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->addKey(['active', 'banned']);
        $this->forge->addKey(['public', 'mustapprove']);
        $this->forge->addUniqueKey('slug');
        $this->forge->addForeignKey('creatorid', 'users', 'id');
        $this->forge->createTable('organizations', FALSE, ['ENGINE' => 'InnoDB']);
    }

    public function down()
    {
        $this->forge->dropTable('organizations');
    }
}
