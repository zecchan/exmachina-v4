<?php

namespace Xm4\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddLicensePricing extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 30,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'appid' => [
                'type'       => 'VARCHAR',
                'constraint' => '64',
            ],
            'xm4appid' => [
                'type'       => 'VARCHAR',
                'constraint' => '64',
                'null'       => true,
            ],
            // is it a permanent pricing?
            'permanent' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 0
            ],
            'hwbound' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 0
            ],
            'hwchangequota' => [
                'type'       => 'INT',
                'constraint' => 4,
                'default'    => 3
            ],
            // is it a quota pricing?
            'credit' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 0
            ],
            'creditcharge' => [
                'type'       => 'INT',
                'constraint' => 30,
                'null'       => true,
            ],
            // is it a demo pricing?
            'demo' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 0
            ],
            'demodurationdays' => [
                'type'       => 'INT',
                'constraint' => 30,
                'null'       => true,
            ],
            // is it a subscription?
            'subscription' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 0
            ],
            'subscriptionduration' => [
                'type'       => 'INT',
                'constraint' => 30,
                'null'       => true,
            ],
            'subscriptiondurationunit' => [
                'type'       => 'VARCHAR',
                'constraint' => '1',
                'null'       => true,
            ],
            // extend only applicable to subscription
            'extendable' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 0
            ],
            // pricing availability
            'activefrom' => [
                'type'       => 'DATETIME',
            ],
            'activeto' => [
                'type'       => 'DATETIME',
            ],
            'active' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 0
            ],
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->addKey('active');
        $this->forge->addKey('appid');
        $this->forge->addForeignKey('xm4appid', 'xm4apps', 'id');
        $this->forge->createTable('licensepricings', FALSE, ['ENGINE' => 'InnoDB']);
    }

    public function down()
    {
        $this->forge->dropTable('licensepricings');
    }
}
