<?php

namespace Xm4\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddXm4App extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'          => [
                'type'       => 'VARCHAR',
                'constraint' => '64',
            ],
            'name' => [
                'type'       => 'VARCHAR',
                'constraint' => '64',
            ],
            'developer' => [
                'type'       => 'VARCHAR',
                'constraint' => '64',
            ],
            'publisher' => [
                'type'       => 'VARCHAR',
                'constraint' => '64',
            ],
            'version' => [
                'type'       => 'VARCHAR',
                'constraint' => '10',
            ],
            'showonstore' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 0
            ],
            'active' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 0
            ],
            'installedon' => [
                'type'       => 'DATETIME',
            ],
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->addKey('active');
        $this->forge->createTable('xm4apps', FALSE, ['ENGINE' => 'InnoDB']);
    }

    public function down()
    {
        $this->forge->dropTable('xm4apps');
    }
}
