<?php

namespace Xm4\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddOrganizationInvites extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 30,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'userid'          => [
                'type'           => 'INT',
                'constraint'     => 30,
                'unsigned'       => true,
                'null'           => true,
            ],
            'organizationid'          => [
                'type'           => 'INT',
                'constraint'     => 30,
                'unsigned'       => true,
                'null'           => true,
            ],
            'isinvite' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 0
            ],
            'invitedby'          => [
                'type'           => 'INT',
                'constraint'     => 30,
                'unsigned'       => true,
                'null'           => true,
            ],
            'isrequest' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 0
            ],
            'requestedat' => [
                'type'       => 'DATETIME',
                'null'       => true
            ],
            'answeredat' => [
                'type'       => 'DATETIME',
                'null'       => true
            ],
            'isaccepted' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 0
            ],
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->addForeignKey('userid', 'users', 'id');
        $this->forge->addForeignKey('organizationid', 'organizations', 'id');
        $this->forge->addForeignKey('invitedby', 'users', 'id');
        $this->forge->createTable('organizationinvites', FALSE, ['ENGINE' => 'InnoDB']);
    }

    public function down()
    {
        $this->forge->dropTable('organizationinvites');
    }
}
