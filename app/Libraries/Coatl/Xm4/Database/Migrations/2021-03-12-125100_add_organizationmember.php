<?php

namespace Xm4\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddOrganizationMember extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 30,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'userid'          => [
                'type'           => 'INT',
                'constraint'     => 30,
                'unsigned'       => true,
                'null'           => true,
            ],
            'organizationid'          => [
                'type'           => 'INT',
                'constraint'     => 30,
                'unsigned'       => true,
                'null'           => true,
            ],
            'active' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 0
            ],
            'banned' => [
                'type'       => 'TINYINT',
                'constraint' => 1,
                'default'    => 0
            ],
            'joinedat' => [
                'type'       => 'DATETIME',
                'null'       => true
            ],
            'bannedat' => [
                'type'       => 'DATETIME',
                'null'       => true
            ],
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->addKey(['active', 'banned']);
        $this->forge->addForeignKey('userid', 'users', 'id');
        $this->forge->addForeignKey('organizationid', 'organizations', 'id');
        $this->forge->createTable('organizationmembers', FALSE, ['ENGINE' => 'InnoDB']);
    }

    public function down()
    {
        $this->forge->dropTable('organizationmembers');
    }
}
