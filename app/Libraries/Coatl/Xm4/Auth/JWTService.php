<?php

namespace Xm4\Auth;

use CodeIgniter\HTTP\RequestInterface;
use Exception;

class JWTService
{
    public function verifyRequest(RequestInterface $request)
    {
        $authHeader = $request->header('Authorization');
        if (!isset($authHeader)) return false;
        $ah = explode(" ", $authHeader->getValue(), 2);
        if (count($ah) != 2 || strtolower($ah[0]) != "bearer") return false;
        $token = $ah[1];
        return $this->verifyToken($token);
    }

    public function verifyToken(string $token, string $authMethod = "JWT") {
        $cfg = new \Config\Xm4\Server();
        try{
            $jwtParts = explode(".", $token, 3);
            $p1 = json_decode($this->base64url_decode($jwtParts[0]));
            $p2 = json_decode($this->base64url_decode($jwtParts[1]), true);
            $p3 = bin2hex($this->base64url_decode($jwtParts[2]));
        }
        catch(Exception $ex)
        {
            return false;
        }
        if (!isset($p1->alg) || $p1->alg != "HS256" || !isset($p1->typ) || $p1->typ != "JWT") return null;
        
        $cmp = hash_hmac("sha256", $jwtParts[0] . "." . $jwtParts[1], $cfg->secretKey);
        if ($p3 != $cmp) return false;


        $aud = $p2["aud"] ?? "";
        if ($aud != $cfg->domainName) return false;

        if (isset($p2["nbf"])) {
            $nbf = $p2["nbf"];
            if ($nbf > time()) return false;
        }
        
        if (isset($p2["exp"])) {
            $exp = $p2["exp"];
            if ($exp < time()) return false;
        }

        $claims = [];
        if (isset($p2["sub"]) && isset($p2["uvc"]) && isset($p2["jti"]))
        {
            // load user claims
            $svc = new UserAuthService();
            $claims = $svc->getClaims($p2["sub"], $p2["uvc"], $p2["jti"]);
            if ($claims === FALSE) {
                return new ClaimsIdentity();
            }
        }
        else return new ClaimsIdentity();

        $id = new ClaimsIdentity($authMethod, $claims);
        return $id;
    }

    public function createToken(array $data, int $exp = null, bool $ref = null, string $jti = null, int $nbf = null, string $aud = null) {
        $cfg = new \Config\Xm4\Server();

        $data["aud"] = $aud ?? $cfg->domainName;
        $data["iss"] = $cfg->domainName;
        $data["iat"] = time();

        if (isset($exp)) {
            if ($exp == 0)
                $data["exp"] = time() + $cfg->jwtExpiration;
            else
                $data["exp"] = $exp;
        }

        if (isset($nbf))
            $data["nbf"] = $nbf;
        
        if (isset($ref)) 
            $data["ref"] = $ref;
        
        if (isset($jti)) 
            $data["jti"] = $jti;

        $p1 = $this->base64url_encode(\json_encode(["alg" => "HS256", "typ" => "JWT"]));
        $p2 = $this->base64url_encode(\json_encode($data));
        $p3 = $this->base64url_encode(hash_hmac("sha256", $p1 . "." . $p2, $cfg->secretKey, true));
        return $p1 . "." . $p2 . "." . $p3;
    }
    function base64url_encode($data) {
      return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }
    function base64url_decode($data) {
      return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

    public function makeUserToken($usr, int $exp = null) {
        $cfg = new \Config\Xm4\Server;
        $jti = \Xm4\Helper::randomString(32);
        $o = [
            "sub"  => $usr->username . '@' . $cfg->domainName,
            "name" => $usr->username,
            "givenname" => $usr->givenname,
            "middlename" => $usr->middlename,
            "surname" => $usr->surname,
            "fullname" => $usr->givenname . ($usr->middlename != "" ? " " . $usr->middlename : "") . ($usr->surname != "" ? " " . $usr->surname : ""),
            "role" => $usr->superuser ? "su" : "user",
            "uvc" => hash("sha256", $usr->username . '@' . $cfg->domainName . $usr->secret . $cfg->staticKey . $jti),
            "jti" => $jti            
        ];

        return $this->createToken($o, $exp);
    }
}