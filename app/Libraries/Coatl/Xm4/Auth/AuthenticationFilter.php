<?php

namespace Xm4\Auth;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class AuthenticationFilter implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        $ap = \Xm4\BaseController::$authPrincipal = \Xm4\BaseController::$authPrincipal ?? new ClaimsPrincipal();

        $cfg = new \Config\Xm4\Server();
        if (isset($cfg) && is_array($cfg->auth)) {
            foreach($cfg->auth as $auth) {
                // Check JWT
                if ($auth == "jwt") {
                    $svc = new JWTService();
                    if ($id = $svc->verifyRequest($request)) {
                        $ap->addIdentity($id);
                    }
                }

                // Check Cookie
                if ($auth == "jwt-cookie") {
                    $svc = new JWTCookieService();
                    if ($id = $svc->verifyRequest($request)) {
                        $ap->addIdentity($id);
                    }
                }

                // Check Basic
            }
        }
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        $ap = \Xm4\BaseController::$authPrincipal ?? new ClaimsPrincipal();
        $cfg = new \Config\Xm4\Server();
        foreach($ap->identities as $id) {
            if ($id->authenticationType == "JWT-Cookie" && $id->isAuthenticated && isset($id->claims["id"])) {
                $cookie = $response->getCookie("token");
                if (!$cookie || !$cookie["value"]) continue;
                $mdl = new \Xm4\Models\UserModel();                
                $u = $mdl->find($id->claims["id"]);
                if (isset($u)) {
                    $svc = new JWTService();
                    $token = $svc->makeUserToken($u);
                    $response->setCookie('token', $token, $cfg->jwtExpiration);
                }
            }
        }
    }
}