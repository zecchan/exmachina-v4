<?php
namespace Xm4\Auth;

use CodeIgniter\HTTP\ResponseInterface;

/**
 * Class UserAuthService
 *
 * Provides a way to authenticate and authorize a user
 * 
 */

class UserAuthService {
    /**
     * get claims from sub
     * 
     * @return array|bool
     */
    public function getClaims(string $sub, string $uvc = null, string $jti = null) {
        $cfg = new \Config\Xm4\Server();
        $spl = explode("@", $sub, 2);
        if (count($spl) != 2 || $spl[1] != $cfg->domainName)
            return false;
        $mdl = new \Xm4\Models\UserModel();
        $u = $mdl->where('username', $spl[0])->first();
        if ($u) {
            if (!isset($uvc) || !isset($jti))
                return $this->generateClaims($u->id);
            $cmp = hash("sha256", $sub . $u->secret . $cfg->staticKey . $jti); 
            if ($cmp == $uvc)
                return $this->generateClaims($u->id);
        }
        return false;
    }

    public function generateClaims(int $uid) {
        $mdl = new \Xm4\Models\UserModel();
        $u = $mdl->find($uid);
        if (!isset($u)) return false;
        if ($u->active != 1 || $u->banned == 1) return false;
        $claims = [
            "id" => $u->id,
            "name" => $u->username,
            "givenname" => $u->givenname,
            "middlename" => $u->middlename,
            "surname" => $u->surname,
            "fullname" => $u->givenname . ($u->middlename != "" ? " " . $u->middlename : "") . ($u->surname != "" ? " " . $u->surname : ""),
            "email" => $u->email,
            "role" => $u->superuser ? "su" : "user",
        ];
        return $claims;
    }
}