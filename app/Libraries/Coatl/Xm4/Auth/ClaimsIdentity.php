<?php
namespace Xm4\Auth;

use CodeIgniter\HTTP\ResponseInterface;

/**
 * Class ClaimsIdentity
 *
 * ClaimsIdentity is a concrete implementation of a claims-based identity; that is, an identity described by a collection of claims.
 * 
 */

class ClaimsIdentity {
    /**
     * The default issuer; "LOCAL AUTHORITY".
     * 
     * @var string
     */
    public $defaultIssuer = "LOCAL AUTHORITY";

    /**
     * Gets the claim type that is used to determine which claims provide the value for the Identity property of this claims identity.
     * 
     * @var string
     */
    public $idClaimType = "id";

    /**
     * Gets the claim type that is used to determine which claims provide the value for the Name property of this claims identity.
     * 
     * @var string
     */
    public $nameClaimType = "name";

    /**
     * Gets the claim type that is used to determine which claims provide the value for the Role property of this claims identity.
     * 
     * @var string
     */
    public $roleClaimType = "role";

    /**
     * Gets the authentication type.
     * 
     * @var string|null
     */
    public $authenticationType = null;

    /**
     * Gets the authorization token that was used to create this claims identity.
     * 
     * @var string|null
     */
    public $authenticationContext = null;

    /**
     * Gets the claims associated with this claims identity.
     * 
     * @var array
     */
    public $claims = [];

    /**
     * Gets a value that indicates whether the identity has been authenticated.
     * 
     * @var bool
     */
    public $isAuthenticated = false;

    public function __construct(string $authenticationType = null, array $claims = null)
    {
        $this->claims = $claims ?? [];
        $this->authenticationType = $authenticationType;
        $this->isAuthenticated = isset($authenticationType);
    }

    /**
     * Gets the identity of this claims identity.
     * 
     * @return string|null
     */
    public function getId() {
        if (isset($this->claims[$this->idClaimType]))
            return $this->claims[$this->idClaimType];
        return null;
    }

    /**
     * Gets the name of this claims identity.
     * 
     * @return string|null
     */
    public function getName() {
        if (isset($this->claims[$this->nameClaimType]))
            return $this->claims[$this->nameClaimType];
        return null;
    }

    /**
     * Gets the role of this claims identity.
     * 
     * @return string|null
     */
    public function getRole() {
        if (isset($this->claims[$this->roleClaimType]))
            return $this->claims[$this->roleClaimType];
        return null;
    }

    /**
     * Adds claim(s) to this claims identity.
     * 
     * @param array|string $claim
     * @param mixed $value
     */
    public function addClaim(mixed $claim, mixed $value) {
        if (is_array($claim)) {
            foreach($claim as $k => $v) {
                $this->claims[$k] = $v;
            }
        } else {
            $this->claims[$claim] = $value;
        }
    }

    /**
     * Retrieves the first claim with the specified claim type.
     * 
     * @param string $claim
     * 
     * @return mixed|null
     */
    public function findFirst(string $claim) {
        if (isset($this->claims[$claim]))
            return $this->claims[$claim];
        return null;
    }
}