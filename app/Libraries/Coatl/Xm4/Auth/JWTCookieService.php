<?php

namespace Xm4\Auth;

use CodeIgniter\HTTP\RequestInterface;
use Exception;

class JWTCookieService
{
    public function verifyRequest(RequestInterface $request)
    {
        $token = $request->getCookie('token');
        if (!isset($token)) return false;
        $jwt = new JWTService($request);
        return $jwt->verifyToken($token, "JWT-Cookie");
    }
}