<?php
namespace Xm4\Auth;

use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\UserAgent;

/**
 * Class AccessLevel
 *
 * AccessLevel is a service that handles authorization, method, requirements, etc. before the main logic is run.
 * 
 */

class AccessLevel {

    protected $request;
    protected $response;
    protected $principal;

    public function __construct(RequestInterface $request, ResponseInterface $response, \Xm4\Auth\ClaimsPrincipal $principal) {
        $this->request = $request;
        $this->response = $response;
        $this->principal = $principal;
        $this->isOptions = $request->getMethod(true) == "OPTIONS";
    }

    /**
     * Contains array of claim type and value that must be found and matched with user's claims.
     * 
     * @var array
     */
    protected $claims = [];

    /**
     * Request checking of a certain claim type.
     * 
     * @param string $claim
     * @param mixed|null $value When ommitted or set to null, only checks whether the claim is exists or not
     * 
     * @return AccessLevel
     */
    public function claim(string $claim, $value = null) {
        $this->claims[] = ["claim" => $claim, "value" => $value];
        return $this;
    }


    /**
     * Contains array of methods that must matched the request method. Also defines allow-methods header.
     * 
     * @var array
     */
    protected $methods = [];

    /**
     * Request checking of a method.
     * 
     * @param string|array $method Method name or array of method name.
     * 
     * @return AccessLevel
     */
    public function method($method) {
        if (is_array($method)) {
            foreach($method as $m)
                $this->method($m);
        }
        if (isset($method) && is_string($method) && $method != "") {
            $method = strtoupper($method);
            if (!in_array($method, $this->methods))
                $this->methods[] = $method;
        }
        return $this;
    }


    /**
     * Contains array of origins that must matched the request method. Also defines allow-origin header.
     * 
     * @var array
     */
    protected $origins = [];

    /**
     * Request checking of an origin.
     * 
     * @param string $origin Allowed origin. '*' - all origins. '.' - this host only
     * 
     * @return AccessLevel
     */

    public function origin(string $origin) {
        if (isset($origin) && is_string($origin) && $origin != "") {
            if ($origin == "*") {
                if (!in_array($origin, $this->origins))
                    $this->origins[] = $origin;
            } else if ($origin == ".") {
                $origin = base_url();
                if (!in_array($origin, $this->origins))
                    $this->origins[] = $origin;
            } else {
                // lookup db
                $db = \Config\Database::connect();
                $org = $db->table('origins')->where('id', $origin)->limit(1)->get()->getResultArray();
                if (count($org) > 0) {
                    $exp = explode(';', $org["origins"]);
                    foreach($exp as $e)
                        $this->origins[] = $e;
                } else {
                    $this->origins[] = $origin;
                }
            }
        }
        return $this;
    }

    public $mustAuthenticated = false;

    public $mustSuper = false;

    /**
     * Request checking of user authorization.
     * 
     * @param bool $super When set to true, the user must be a super user.
     * 
     * @return AccessLevel
     */
    public function authorize(bool $super = false) {
        $this->mustAuthenticated = true;
        $this->mustSuper = $super;
        return $this;
    }

    /**
     * Defines whether the current request is an OPTIONS request or not
     * 
     * @var bool
     */
    public $isOptions = false;

    /**
     * Defines whether the current controller is an API controller or not
     * 
     * @var bool
     */
    public $isAPIController = false;

    /**
     * Defines the default error view when a user access is fail and the controller is not an API controller. If null, it will let the controller handle the error.
     * 
     * @var string|null
     */
    public $errorView = null;

    /**
     * Automatically handles preflight requests. Default is true.
     * 
     * @var bool
     */
    public $handlePreflight = true;

    /**
     * Automatically handles no-access requests. Default is true;
     * 
     * @var bool
     */
    public $handleFail = true;

    /**
     * Performs access check
     */
    public function check(bool $hFail = true, bool $hPreflight = true) {
        $this->handleFail = $hFail;
        $this->handlePreflight = $hPreflight;
        $m = strtoupper($this->request->getMethod());
        if (count($this->methods) > 0 && !in_array($m, $this->methods))
            return $this->checkFailed(405, ["Allow" => $this->methods], "Method is not allowed", ["allow" => $this->methods]);
        
        $req_origin = $this->request->getHeaderLine('Origin');
        if (count($this->origins) > 0 && !(in_array("*", $this->origins) || in_array($req_origin, $this->origins))) {
            return $this->checkFailed(400, null, "Origin is not allowed");
        }

        if ($this->mustAuthenticated) {
            if (!$this->principal->isAuthenticated()) 
                return $this->checkFailed(401, null, "You are not authorized to access this resource");
            if ($this->mustSuper && !$this->principal->isSuperUser())
                return $this->checkFailed(403, null, "You are not allowed to access this resource");
        }

        if (count($this->claims) > 0) {
            foreach($this->claims as $ck => $cv) {
                $v = $this->principal->hasClaim($ck, $cv);
                if ($v == false) 
                    return $this->checkFailed(403, null, "You are not allowed to access this resource");
            }
        }

        if ($m == "OPTIONS" && $this->handlePreflight) {
            // returns preflight response
            $this->response->setStatusCode(204);
            if (count($this->origins) == 0 || in_array("*", $this->origins) || in_array($req_origin, $this->origins)) {
                $this->setHeader("Access-Control-Allow-Origin", $req_origin);
                $this->setHeader("Vary", ["Origin"]);
            }
            if (count($this->methods) > 0)
                $this->setHeader("Access-Control-Allow-Methods", $this->methods);
            $this->setHeader("Access-Control-Max-Age", 86400);
            $this->response->send();
            die();
        }
        return true;
    }

    protected function setHeader($header, $value) {
        if (is_array($value)) {
            foreach($value as $v) {
                if ($this->response->header($header) != null)
                    $this->response->appendHeader($header, $v);
                else
                    $this->response->setHeader($header, $v);
            }
        }
        else
            $this->response->setHeader($header, $value);
    }

    protected function checkFailed($code = null, array $headers = null, string $message = null, $data = null) {
        if ($this->handleFail) {
            if (isset($code))
                $this->response->setStatusCode(intval($code));
            if (isset($headers) && is_array($headers)) {
                foreach($headers as $hk => $hv) {
                    $this->setHeader($hk, $hv);
                } 
            }
            if ($this->isAPIController) {
                $o = [ "statusCode" => $code ];
                if (isset($message)) $o["message"] = $message;
                if (isset($data)) $o["data"] = $data;
                $this->response->setBody(\json_encode($o));
                $this->setHeader("Content-Type", "application/json");
            }
            $this->response->send();
            die();
        }
        return false;
    }
}