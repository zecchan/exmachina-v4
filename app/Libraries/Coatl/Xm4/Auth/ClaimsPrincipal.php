<?php
namespace Xm4\Auth;

use CodeIgniter\HTTP\ResponseInterface;

/**
 * Class ClaimsPrincipal
 *
 * ClaimsPrincipal contains a list claims that a user have.
 * 
 */

class ClaimsPrincipal {
    
    /**
     * Gets a collection that contains all of the claims identities associated with this claims principal.
     * 
     * @var array
     */
    public $identities = [];

    /**
     * User authentication and authorization service
     * 
     * @param UserAuthService $UAS
     */
    private $UAS;

    public function __construct()
    {
        $this->UAS = new UserAuthService();
    }

    /**
     * Adds the unauthorized identity to this claims principal.
     */
    public function addUnauthorizedIdentity() {
        $this->identities[] = new ClaimsIdentity();
    }

    /**
     * Adds the specified claims identity to this claims principal.
     * 
     * @param ClaimsIdentity $identity
     */
    public function addIdentity(ClaimsIdentity $identity) {
        $this->identities[] = $identity;
    }

    /**
     * Gets a collection that contains all of the claims from all of the claims identities associated with this claims principal.
     * 
     * @return array
     */
    public function getClaims() {
        $arr = [];
        foreach($this->identities as $i) {
            foreach($i->claims as $k => $v)
                $arr[$k] = $v;
        }
        return $arr;
    }

    /**
     * Retrieves the first claim with the specified claim type.
     * 
     * @param string $claim
     * 
     * @return mixed|null
     */
    public function findFirst(string $claim) {
        foreach($this->identities as $i) {
            $re = $i->findFirst($claim);
            if ($re != null) return $re;
        }
        return null;
    }

    /**
     * Determines whether any of the claims identities associated with this claims principal contains a claim with the specified claim type and value.
     * 
     * @param string $claim
     * @param mixed|null $value
     * 
     * @return bool
     */
    public function hasClaim(string $claim, $value = null) {
        foreach($this->identities as $i) {
            $re = $i->findFirst($claim);
            if ($re != null && ($value == null || $re == $value)) return true;
        }
        return false;
    }

    /**
     * Determines whether any of the claims identities associated with this claims principal is authenticated.
     * 
     * @param string $claim
     * @param mixed|null $value
     * 
     * @return bool
     */
    public function isAuthenticated() {
        foreach($this->identities as $i) {
            if ($i->isAuthenticated) return true;
        }
        return false;
    }

    /**
     * Gets the id of this claims principal.
     * 
     * @return string|null
     */
    public function getId() {
        foreach($this->identities as $i) {
            $re = $i->getId();
            if ($re != null) return $re;
        }
        return null;
    }

    /**
     * Gets the name of this claims principal.
     * 
     * @return string|null
     */
    public function getName() {
        foreach($this->identities as $i) {
            $re = $i->getName();
            if ($re != null) return $re;
        }
        return null;
    }

    /**
     * Gets the role of this claims principal.
     * 
     * @return string|null
     */
    public function getRole() {
        foreach($this->identities as $i) {
            $re = $i->getRole();
            if ($re != null) return $re;
        }
        return null;
    }

    /**
     * Checks whether this identity is a super user or not.
     * 
     * @return bool
     */
    public function isSuperUser() {
        return $this->getRole() == "su";
    }
}