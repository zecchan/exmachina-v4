<?php
namespace Xm4;

class Helper {
    /**
     * Generates a random string
     * 
     * @return string
     */
    public static function randomString(int $len, string $charset = null) {
        $charset = $charset ?? "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*(),./;'\\\":?><";
        $re = "";

        for($i = 0; $i < $len; $i++) {
            $ix = rand(0, strlen($charset) - 1);
            $re .= substr($charset, $ix, 1);
        }
        return $re;
    }
    /**
     * Generates a random alphanumeric string
     * 
     * @return string
     */
    public static function randomAlphanum(int $len, string $charset = null) {
        $charset = $charset ?? "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
        $re = "";

        for($i = 0; $i < $len; $i++) {
            $ix = rand(0, strlen($charset) - 1);
            $re .= substr($charset, $ix, 1);
        }
        return $re;
    }
    /**
     * Generates a random hex string
     * 
     * @return string
     */
    public static function randomHex(int $len, bool $upper = false) {
        $charset = "ABCDEF0123456789";
        $re = "";
        for($i = 0; $i < $len; $i++) {
            $ix = rand(0, strlen($charset) - 1);
            $re .= substr($charset, $ix, 1);
        }
        if (!$upper) $re = strtolower($re);
        return $re;
    }

    /**
     * Converts string to array
     * 
     * @return array
     */
    public static function stringToVersion(string $version) {
        $ar = explode(".", $version);
        $re = [];
        foreach($ar as $a) {
            $re[] = intval($a);
        }
        return $re;
    }

    /**
     * Compares version: -1 if $v1 is less than $v2, 1 if $v1 is greater than $v2, 0 if same
     * 
     * @return int|false
     */
    public static function compareVersion($v1, $v2) {
        if (is_string($v1)) $v1 = Helper::stringToVersion($v1);
        if (is_string($v2)) $v2 = Helper::stringToVersion($v2);
        if (!is_array($v1) || !is_array($v2)) return false;
        for($i = 0; $i < max(count($v1), count($v2)); $i++) {
            if (count($v1) <= $i) return -1;
            if (count($v2) <= $i) return 1;
            if ($v1[$i] < $v2[$i]) return -1;
            if ($v1[$i] > $v2[$i]) return 1;
        }
        return 0;
    }
}