<?php
namespace Xm4\Xm4a;

use Exception;

/**
 * This class functions as the installer of xm4a packages
 */
class Xm4aInstaller {

    /**
     * Extracts a zip file to specified path.
     * 
     * @return string|bool The path where the files are extracted
     */
    public function extractTo(string $package, string $path = null) {
        if (!isset($path))
            $path = WRITEPATH . "temp/" . \Xm4\Helper::randomAlphanum(32);
        if (!\file_exists($path))
            mkdir($path, 0777, true);
        $zip = new \ZipArchive;
        if ($zip->open($package) === TRUE) {
            $zip->extractTo($path);
            $zip->close();
        } else return false;
        return $path;
    }

    /**
     * Reads manifest.json from a certain path.
     * 
     * @return object|bool
     */
    public function readManifest(string $path) {
        $jsPath = $path . "/manifest.json"; 
        if (!\file_exists($jsPath)) return false;
        return json_decode(file_get_contents($jsPath));
    }

    /**
     * Installs an extracted app.
     * 
     * @return array|string
     */
    public function installExtracted(string $path, string $appId) {
        $appNamespace = implode("/", explode(".", $appId));
        $dirs = [
            "controller" => APPPATH . "Controllers",
            "api" => APPPATH . "Controllers/Api",
            "config" => APPPATH . "Config/Xm4",
            "public" => realpath(APPPATH . "/../public"),
            "migration" => APPPATH . "Database/Migration",
            "lib" => APPPATH . "Libraries",
        ];
        $rollback = false;
        $rollbackReason = "";
        $log = [];
        foreach($dirs as $f => $t) {
            $srcPath = $path . "/" . $f;
            $dstPath = $t . ($f == "public" || $f == "migration" || $f == "config" || $f == "lib" ? "" : "/" . $appNamespace);
            if (!\file_exists($srcPath)) continue;
            $res = $this->recursiveCopy($srcPath, $dstPath);
            if (!is_array($res)) {
                $rollback = true;
                $rollbackReason = "Failed when copying file(s): " . $res;
                break;
            }
            $log[$f] = $res;
        }

        if ($rollback) {
            foreach($log as $l) {
                foreach($l as $f)
                    unlink($f);
            }
            return $rollbackReason;
        }
        return $log;
    }

    /**
     * Recursively copy the contents of a directory to other directory.
     * 
     * @return array|string
     */
    public function recursiveCopy(string $from, string $to) {
        $copied = [];
        $rollback = false;
        $rollbackReason = "";

        if (!\file_exists($to)) {
            try{
                mkdir($to, 0777, true);
            }
            catch(\Exception $ex)
            {
                return $ex->getMessage() . " => " . $to;
            }
        }
        if ($files = scandir($from)) {
            foreach($files as $f) {
                if ($f == "." || $f == "..") continue;
                $fpath = $from . "/" . $f;
                $tpath = $to . "/" . $f;
                if (is_dir($fpath)) {
                    $res = $this->recursiveCopy($fpath, $tpath);
                    if (is_string($res)) {
                        $rollback = true;
                        $rollbackReason = $res;
                        break;
                    }
                    $copied = array_merge($copied, $res);
                } else {
                    if (\file_exists($tpath)) {
                        $rollback = true;
                        $rollbackReason = "File '$tpath' already exists";
                        break;
                    }
                    copy($fpath, $tpath);
                    $copied[$fpath] = $tpath;
                }
            }
        }

        if ($rollback) {
            foreach($copied as $c) {
                unlink($c);
            }
            return $rollbackReason;
        }
        return $copied;
    }

    /**
     * Install an application package.
     */
    public function install(string $xm4a) {
        $log = [];
        $installLog = [];
        // 1. Extract files
        $path = $this->extractTo($xm4a);
        $log[] = "Extracting package...";
        if ($path == false) {
            $log[] = "Failed to extract package";
            return $this->buildResult(false, $log);
        }
        try {
            // 2. Read manifest
            $log[] = "Reading manifest...";
            $manifest = $this->readManifest($path);
            if (!$manifest) {
                $log[] = "Failed to read manifest";
                return $this->buildResult(false, $log);
            }
            $installLog["manifest"] = $manifest;

            $mdl = new \Xm4\Models\Xm4appModel();
            if (isset($manifest->prerequisites)) {
                foreach($manifest->prerequisites as $p) {
                    $pa = $mdl->find($p->appid ?? "");
                    if (!isset($pa)) {
                        $log[] = "Required app is not installed: " . $p->appid . ' v' . $p->version;
                        return $this->buildResult(false, $log);
                    } else {
                        $vc = \Xm4\Helper::compareVersion($pa['version'], $p->version);
                        if ($vc >= 0) {
                            $log[] = "Prerequisite fulfilled: " . $p->appid . " v" . $pa['version'];
                        } else {
                            $log[] = "Prerequisite failed: " . $p->appid . " v" . $pa['version'] . '(Version >= ' . $p->version .' is required)';
                            return $this->buildResult(false, $log);
                        }
                    }
                }
            }

            try{
                // uninstall old
                $aid = $manifest->app->appid ?? "";
                $app = $mdl->find($aid);

                if (isset($app)) {
                    $vc = \Xm4\Helper::compareVersion($app['version'], $manifest->app->version);
                    if ($vc === false) {
                        $log[] = "Cannot read version";
                        return $this->buildResult(false, $log);
                    }
                    if ($vc >= 0) {
                        $log[] = "Current version is higher or the same";
                        return $this->buildResult(false, $log);
                    }
                    if ($vc < 0) {
                        $log[] = "Uninstalling old version...";
                        $res = $this->uninstall($aid);
                        if (!$res->success) {
                            $log = array_merge($log, $res->log);
                            $log[] = "Uninstall failed...";
                            return $this->buildResult(false, $log);
                        }
                        $log[] = "Uninstall success!";
                    }
                }

                // 3. move files
                $log[] = "Copying files...";
                $mvlog = $this->installExtracted($path, $manifest->app->appid);
                if (is_string($mvlog)) {
                    $log[] = $mvlog;
                    return $this->buildResult(false, $log);
                }
                $delOnUninstall = [];
                $init = null;
                foreach($mvlog as $k => $ml)
                {
                    $log[] = "Copying " . $k . "...";
                    foreach($ml as $f) {
                        $log[] = "Copied [" . $f . "]";
                        $delOnUninstall[] = $f;

                        $md5 = hash_file("md5", $f, true);
                        if ($init != null) {
                            $init = $init ^ $md5;
                        } else {
                            $init = $md5;
                        }
                    }
                }
                $installLog["files"] = $delOnUninstall;
                $integrity = $init ? bin2hex($init) : "none";
                $installLog["checksum"] = $integrity;
                if (isset($manifest->app->checksum) && $integrity != $manifest->app->checksum) {
                    foreach($delOnUninstall as $d) unlink($d);
                    $log[] = "Fatal error: Checksum does not match! Checksum: " . $integrity . ", manifest checksum: " . $manifest->app->checksum;
                    return $this->buildResult(false, $log);
                }

                // 4. Write Configs
                // config should be edited by web admin

                // 5. Migrate database
                $log[] = "Migrating database...";
                $migrate = \Config\Services::migrations();
                $migrate->setNamespace(null);
                try {
                    $migrate->latest();
                    $log[] = "Database migration success!";
                } catch (\Exception $e) {
                    foreach($delOnUninstall as $d) unlink($d);
                    $log[] = "Database migration failed: " . $e->getMessage();
                    return $this->buildResult(false, $log);
                }

                // Insert App to DB
                $log[] = "Registering application...";
                $data = [
                    "id" => $manifest->app->appid,
                    "name" => $manifest->app->name,
                    "developer" => $manifest->app->developer,
                    "publisher" => $manifest->app->publisher,
                    "version" => $manifest->app->version,
                    "installedon" => date("Y-m-d H:i:s")
                ];
                $saveRes = isset($app) ? $mdl->update($manifest->app->appid, $data) : $mdl->insert($data);
                if ($saveRes === false) {
                    foreach($delOnUninstall as $d) unlink($d);
                    $log[] = "Fatal error: failed when updating database";
                    return $this->buildResult(false, $log);
                }

                // Finalize
                $log[] = "Writing uninstall information...";
                $logPath = WRITEPATH . "installs";
                if (!\file_exists($logPath)) mkdir($logPath, 0777, true);
                $logPath .= "/" . $manifest->app->appid . ".json";
                if (\file_exists($logPath)) unlink($logPath);
                $installJson = \json_encode($installLog);
                $f = fopen($logPath, "w");
                fwrite($f, $installJson);
                fclose($f);

                $log[] = "Installation completed, details are saved to " . $logPath;
                return $this->buildResult(true, $log);
            }
            catch(\Exception $ex) {
                $log[] = "Fatal error: " . $ex->getMessage();
                return $this->buildResult(false, $log);
            }
        }
        finally {
            $this->rrmdir($path);
        }
    }

    /**
     * Uninstall an application package.
     */
    public function uninstall(string $appId) {
        $appId = $appId ?? "";
        $log = [];
        try{
            $log[] = "Finding application...";
            $mdl = new \Xm4\Models\Xm4appModel();
            $app = $mdl->find($appId);
            if (!isset($app)) {
                $log[] = "Error: Application is not found!";
                return $this->buildResult(false, $log);
            }
            $log[] = "Uninstalling [" . $app['name'] . "] v" . $app['version'] . "...";
            $log[] = "Finding uninstallation file...";
            $path =  WRITEPATH . "installs/" . $app['id'] . ".json";
            if (!\file_exists($path)) {
                $log[] = "Fatal Error: Uninstallation file is not found!";
                return $this->buildResult(false, $log);
            }
            $fc = file_get_contents($path);
            $json = json_decode($fc);
            if (!isset($json) || !isset($json->files)) {
                $log[] = "Fatal Error: Uninstallation file is corrupted!";
                return $this->buildResult(false, $log);
            }
            $log[] = "Removing files...";
            foreach($json->files as $d) unlink($d);
            $log[] = "Unregistering application...";
            if ($mdl->where('id', $app['id'])->delete() === false) {
                $log[] = "Fatal Error: Failed to unregister application";
                return $this->buildResult(false, $log);
            }
            $log[] = "Uninstalled successfully";
            return $this->buildResult(true, $log);
        }
        catch(Exception $ex) {
            $log[] = "Fatal error: " . $ex->getMessage();
            return $this->buildResult(false, $log);
        }
    }

    function buildResult(bool $isSuccess, array $log) {
        $res = [
            "success" => $isSuccess,
            "log" => $log,
        ];
        return (object)$res;
    }

    function rrmdir(string $src) {
        if($src == "" || $src == "." || $src == "..") return;
        $dir = opendir($src);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                $full = $src . '/' . $file;
                if ( is_dir($full) ) {
                    $this->rrmdir($full);
                }
                else {
                    unlink($full);
                }
            }
        }
        closedir($dir);
        rmdir($src);
    }
}