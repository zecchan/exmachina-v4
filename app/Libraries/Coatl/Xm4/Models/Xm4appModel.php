<?php

namespace Xm4\Models;

use CodeIgniter\Model;

class Xm4appModel extends Model
{
    protected $table      = 'xm4apps';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';

    protected $allowedFields = ['id', 'name', 'developer', 'publisher', 'version', 'showonstore', 'active', 'installedon'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $useSoftDeletes = false;
    protected $deletedField  = 'deleted_at';

    protected $skipValidation     = false;
    protected $validationRules    = [
        'id'         => 'required|min_length[2]|max_length[64]|appid',
        'name'       => 'required|min_length[2]|max_length[64]',
        'developer'  => 'required|min_length[2]|max_length[64]',
        'publisher'  => 'required|min_length[2]|max_length[64]',
        'version'    => 'required|min_length[3]|regex_match[/^[0-9]+(\\.[0-9]+)+$/]',
    ];
    protected $validationMessages = [];
}