<?php

namespace Xm4\Models;

use CodeIgniter\Model;

class OrganizationInviteModel extends Model
{
    protected $table      = 'organizationinvites';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';

    protected $allowedFields = [
                                'userid', 'organizationid', 'invitedby', 'isrequest', 'requestedat', 'answeredat', 'isaccepted'
                               ];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $useSoftDeletes = false;
    protected $deletedField  = 'deleted_at';

    protected $skipValidation     = false;
    protected $validationRules    = [
        'userid'    => 'required|integer',
        'organizationid'    => 'required|integer',
        'isrequest'    => 'integer',
        'isaccepted'    => 'integer',
    ];
    protected $validationMessages = [];

    /**
     * Create an invitation to join an organization
     * 
     * @return string|true
     */
    public function makeInvite(int $inviter, int $userId, int $organizationId) {
        $mdlu = new UserModel();
        if ($mdlu->valid($inviter) == null) return "Invalid inviter";
        if ($mdlu->valid($userId) == null) return "Invited user is not found";
        if ($inviter == $userId) return "User cannot invite themselves";
        $inv = $this->where('answeredat IS NULL')->where('organizationid', $organizationId)->where('userid', $userId)->first();
        if (isset($inv)) return "User already invited";
        $mdlmem = new OrganizationMemberModel();
        if (!$mdlmem->isMemberOfOrganization($inviter, $organizationId)) return "Inviter does not belong to the organization";
        if ($mdlmem->isMemberOfOrganization($userId, $organizationId)) return "User already a member of the organization";
        $data = [
            'userid' => $userId,
            'organizationid' => $organizationId,
            'invitedby' => $inviter,
            'isrequest' => 0,
            'requestedat' => date('Y-m-d H:i:s'),
            'isaccepted' => 0,
        ];
        if (!$this->insert($data)) {
            return $this->errors();
        }
        return true;
    }

    /**
     * Accept an invitation to join an organization
     * 
     * @return string|true
     */
    public function acceptInvite(int $inviteId, int $userId, bool $accept = true) {
        $mdlu = new UserModel();
        if ($mdlu->valid($userId) == null) return "Invited user is not found";
        $inv = $this->where('isrequest', 0)->where('id', $inviteId)->where('userid', $userId)->first();
        if (!isset($inv)) return "Invitation not found";
        if (isset($inv['answeredat'])) return true;

        $mdlmem = new OrganizationMemberModel();
        $prev = $mdlmem->where('userid', $inv['userid'])->where('organizationid', $inv['organizationid'])->first();
        if (isset($prev)) return "User already a member of the organization";

        if (isset($inv['invitedby'])) {
            if (!$mdlmem->isMemberOfOrganization(intval($inv['invitedby']), intval($inv['organizationid']))) return "Inviter does not belong to the organization";
        }

        $inv['answeredat'] = date('Y-m-d H:i:s');
        $inv['isaccepted'] = $accept ? 1 : 0;
        $this->update($inv['id'], $inv);

        $data = [
            'userid' => $inv['userid'],
            'organizationid' => $inv['organizationid'],
            'active' => 1,
            'banned' => 0,
            'joinedat' => date('Y-m-d H:i:s'),
            'bannedat' => null
        ];
        $mdlmem->insert($data);
        return true;
    }
}