<?php

namespace Xm4\Models;

use CodeIgniter\Model;

class LicenseModel extends Model
{
    protected $table      = 'licenses';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';

    protected $allowedFields = ['userid', 'appid', 'xm4appid', 'serial', 'license', 'hwkey', 'hwchangeleft', 'licensepricingid', 'licensepricingsnapshot', 'active', 'activatedon', 'activeuntil'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $useSoftDeletes = false;
    protected $deletedField  = 'deleted_at';

    protected $skipValidation     = false;
    protected $validationRules    = [
        'appid'      => 'required|min_length[2]|max_length[64]|appid',
        'xm4appid'   => 'max_length[64]|appid',
        'serial'     => 'required|min_length[20]|max_length[20]',
        'license'    => 'max_length[32]',
        'hwkey'      => 'max_length[32]',
        'active'     => 'integer',
    ];
    protected $validationMessages = [];

    public function buildSerial($appid) {
        $serbase = substr(hash('sha256', strtolower($appid)), 0, 4);
        $random = \Xm4\Helper::randomHex(12);
        $ser = $serbase . $random;
        $hash = substr(hash('sha256', $ser), 0, 4);
        $serial = $ser . $hash;
        $ex = $this->where('serial', $serial)->first();
        while(isset($ex)) {
            $random = \Xm4\Helper::randomHex(12);
            $ser = $serbase . $random;
            $hash = substr(hash('sha256', $ser), 0, 4);
            $serial = $ser . $hash;
            $ex = $this->where('serial', $serial)->first();
        }
        return $serial;
    }

    public function buildLicense(string $serial, string $appid, string $hwkey = null) {
        $pureSerial = strtolower(trim(str_replace('-','', $serial)));
        if (isset($hwkey))
            $pureSerial .= strtolower($hwkey);
        $buildbase = $pureSerial . hash('sha256', trim(strtolower($appid)));
        return substr(hash('sha256', $buildbase),0,32);
    }

    public function formatSerial(string $serial) {
        if (strlen($serial) % 4 == 0 && strlen($serial) > 0) {
            $a = [];
            for($i = 0; $i < strlen($serial); $i += 4) {
                $a[] = substr($serial, $i, 4);
            }
            return strtoupper(implode('-', $a));
        } 
        return $serial;
    }

    public function checkSerial(string $serial) {
        $serial = strtolower(str_replace('-', '', $serial));
        if (strlen($serial) != 20) return false;
        $base = substr($serial, 0, 16);
        $hash = substr(hash('sha256', $base), 0, 4);
        $chk = substr($serial, 16);
        return $hash == $chk;
    }

    public function generateSerial(int $licensepricingid, int $count = 1) {
        $mdl = new LicensePricingModel();
        $lp = $mdl->getActiveById($licensepricingid);
        if (!isset($lp)) return "Invalid license pricing id";
        $appid = $lp['appid'];
        $xm4appid = $lp['xm4appid'] ?? null;
        $data = [
            'appid' => $appid,
            'xm4appid' => $xm4appid,
            'licensepricingid' => $lp['id'],
            'licensepricingsnapshot' => \json_encode($lp),
            'active' => 0,
        ];
        $res = [];
        $rollback = [];
        for($i = 0; $i < $count; $i++) {
            $serial = $this->buildSerial($appid);
            $data['serial'] = $serial;
            $id = $this->insert($data, true);
            if ($id) 
            {
                $res[] = $this->formatSerial($serial);
                $rollback[] = $id;
            }
            else
            {
                $this->whereIn('id', $rollback)->delete();
                return 'Failed to register serial';
            }
        }
        return $res;
    }
}