<?php

namespace Xm4\Models;

use CodeIgniter\Model;

class OrganizationMemberModel extends Model
{
    protected $table      = 'organizationmembers';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';

    protected $allowedFields = [
                                'userid', 'organizationid', 'active', 'banned', 'joinedat', 'bannedat'
                               ];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $useSoftDeletes = false;
    protected $deletedField  = 'deleted_at';

    protected $skipValidation     = false;
    protected $validationRules    = [
        'userid'    => 'required|integer',
        'organizationid'    => 'required|integer',
        'banned'    => 'integer',
        'active'    => 'integer',
    ];
    protected $validationMessages = [];

    public function isMemberOfOrganization(int $userId, int $orgId) {
        $m = $this->where('organizationid', $orgId)->where('userid', $userId)->where('active', 1)->where('banned', 0)->first();
        return isset($m);
    }
}