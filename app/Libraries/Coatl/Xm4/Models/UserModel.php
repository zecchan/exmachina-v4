<?php

namespace Xm4\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table      = 'users';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = \Xm4\Entities\User::class;

    protected $allowedFields = ['username', 'email', 'password', 'secret', 'givenname', 'middlename', 'surname', 'superuser', 'active', 'verified', 
                                'banned', 'banreason', 'lastlogin', 'lastlogindata', 'lastactivity', 'lastactivitydata'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $useSoftDeletes = false;
    protected $deletedField  = 'deleted_at';

    protected $skipValidation     = false;
    protected $validationRules    = [
        'username'   => 'required|min_length[3]|max_length[50]|identifier|is_unique[users.username,id,{id}]',
        'email'      => 'required|valid_email|max_length[120]|is_unique[users.email,id,{id}]',
        'password'   => 'required|min_length[8]',
        'givenname'  => 'required|min_length[3]|max_length[100]|names',
        'middlename' => 'max_length[100]|names',
        'surname'    => 'max_length[100]|names',
    ];
    protected $validationMessages = [];

    protected $allowCallbacks = true;
    protected $beforeInsert = ["preSave"];
    protected $beforeUpdate = ["preSave"];

    public static function hashPassword(string $pw, string $secret) {
        $cfg = new \Config\Xm4\Server;
        return hash('sha256', $cfg->staticKey . $pw . $secret);
    }

    public static function comparePassword(string $plain, string $hash, string $secret) {
        return $hash == UserModel::hashPassword($plain, $secret);
    }

    public function preSave($dataArray) {
        foreach($dataArray as $k => $data) {
            if (isset($data["password"])) {
                $data['password'] = UserModel::hashPassword($data['password'], $data['secret']);
            } else unset($data['password']);
            $dataArray[$k] = $data;
        }
        return $dataArray;
    }

    public function valid(int $userId) {
        $u = $this->find($userId);
        if (!isset($u)) return false;
        if ($u->active == 0) return false;
        if ($u->banned == 1) return false;
        return true;
    }
}