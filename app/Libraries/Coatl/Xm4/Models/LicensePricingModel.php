<?php

namespace Xm4\Models;

use CodeIgniter\Model;

class LicensePricingModel extends Model
{
    protected $table      = 'licensepricings';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';

    protected $allowedFields = [
                                'appid', 'xm4appid ', 'permanent', 'credit', 'creditcharge', 'demo', 'demodurationdays', 'hwbound', 'hwchangequota',
                                'subscription', 'subscriptionduration ', 'subscriptiondurationunit', 'extendable', 'activefrom', 'activeto', 'active',
                               ];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $useSoftDeletes = false;
    protected $deletedField  = 'deleted_at';

    protected $skipValidation     = false;
    protected $validationRules    = [
        'appid'      => 'required|min_length[2]|max_length[64]|appid',
        'xm4appid'   => 'required|min_length[2]|max_length[64]|appid',
        'credit'    => 'integer',
        'creditcharge'    => 'integer',
        'demo'    => 'integer',
        'demodurationdays'    => 'integer',
        'subscription'    => 'integer',
        'subscriptionduration'    => 'integer',
        'subscriptiondurationunit'    => 'min_length[1]|max_length[1]',
        'extendable'    => 'integer',
        'active'    => 'integer',
    ];
    protected $validationMessages = [];

    public function getActiveById(int $id) {
        return $this->where('id', $id)
            ->where('activefrom <=', date('Y-m-d H:i:s'))
            ->where('activeto >=', date('Y-m-d H:i:s'))
            ->where('active', 1)->first();
    }
    public function getActiveByApp(string $appid) {
        return $this->where('appid', $appid)
            ->where('activefrom <=', date('Y-m-d H:i:s'))
            ->where('activeto >=', date('Y-m-d H:i:s'))
            ->where('active', 1)->getResultArray();
    }
    public function getActiveByXm4App(string $xm4appid) {
        return $this->where('xm4appid', $xm4appid)
            ->where('activefrom <=', date('Y-m-d H:i:s'))
            ->where('activeto >=', date('Y-m-d H:i:s'))
            ->where('active', 1)->getResultArray();
    }
}