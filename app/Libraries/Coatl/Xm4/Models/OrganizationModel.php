<?php

namespace Xm4\Models;

use CodeIgniter\Model;

class OrganizationModel extends Model
{
    protected $table      = 'organizations';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';

    protected $allowedFields = [
                                'slug', 'name', 'address', 'phone', 'email', 'active', 'banned', 'public', 'mustapprove', 'creatorid', 'created_at', 'updated_at'
                               ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $useSoftDeletes = false;
    protected $deletedField  = 'deleted_at';

    protected $skipValidation     = false;
    protected $validationRules    = [
        'slug'      => 'required|min_length[2]|max_length[64]|identifier|is_unique[organizations.slug,id,{id}]',
        'name'   => 'required|min_length[2]|max_length[64]',
        'banned'    => 'integer',
        'active'    => 'integer',
        'public'    => 'integer',
        'mustapprove'    => 'integer',
    ];
    protected $validationMessages = [];

    public function findBySlug(string $slug) {
        return $this->where('slug', $slug)->first();
    }

    public function getMemberOf($idOrSlug) {
        $mdl = new OrganizationMemberModel();

        $o = $this->where('slug', $idOrSlug)->orWhere('id', $idOrSlug)->first();
        if (isset($o)) {
            return $mdl->where('organizationid', $o['id'])->getResultArray();
        }
        return [];
    }
}