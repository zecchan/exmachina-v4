<?php
namespace Xm4;

/**
 * Class ExMachina
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */

class ExMachina
{
	/**
	 * An array that describes version [major, minor, build].
	 *
	 * @var array
	 */
	public static $version = [4, 0, 0];

    /**
     * Returns version as string
     */
    public static function getVersion() {
        return implode(".", ExMachina::$version);
    }
}
