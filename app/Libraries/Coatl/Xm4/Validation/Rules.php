<?php
namespace Xm4\Validation;

class Rules {
    public function identifier(string $str = null): bool {
        if ($str == null) return false;
        $re = '/^[a-zA-Z][a-zA-Z0-9]*$/';        
        return !preg_match($re, $str) ? false : true;
    }
    
    public function names(string $str = null): bool {
        if ($str == null) return true;
        $re = '/^([a-zA-Z0-9][a-zA-Z0-9\.\'\` ]*)?$/';        
        return !preg_match($re, $str) ? false : true;
    }
    
    public function appid(string $str = null): bool {
        if ($str == null) return true;
        $re = '/^([a-zA-Z0-9][a-zA-Z0-9]*)(\.[a-zA-Z0-9][a-zA-Z0-9]*)$/';        
        return !preg_match($re, $str) ? false : true;
    }
}