# ExMachina 4 Application Package

File extension is *.xm4a

It's a zip file that contains the following structure:

- src/
  - controller/
    - (controller files... It will copied to **APPPATH/Controllers/(appnamespace)/**)
  - config/
    - (config files... It will copied to **APPPATH/Config/Xm4**)
  - api/
    - (api controller files... It will copied to **APPPATH/Controllers/Api/(appnamespace)/**)
  - public/
    - (files in public folder... It will copied to **PUBLICPATH**)
  - migration/
    - (migration files... It will copied to **APPPATH/Database/Migrations**)
  - lib/
    - (library files... It will copied to **APPPATH/Libraries**)
- manifest.json

Any other file will be ignored.

(appnamespace) is your appid turned into path. Example: Coatl.MyApp => Coatl/MyApp

## Installing
1. Extract the files
2. Read manifest.json and check
3. Move files to respective folders
4. Write configs - deprecated, should be written by sysadmin
5. Migrate the database

## Uninstalling
1. Regress the database if necessary
2. Remove installed files (except in lib)

## manifest.json
The structure of manifest.json are the following:

```json
{
    "app": {
        "name": "MyApp",
        "developer": "Code Atelier",
        "publisher": "Code Atelier",
        "appid": "Coatl.MyApp",        
        "version": "1.0",
        "checksum": "d41d8cd98f00b204e9800998ecf8427e"
    },
    "prerequisites": [
        {
            "appid": "Coatl.Xm4",
            "version": "4.0"
        }
    ],
    "uninstall": {
        "regress": false
    },
    "licenseServer": ["https://exmachina.dev/xm4al"]
}
```