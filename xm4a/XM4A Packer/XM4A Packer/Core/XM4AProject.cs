﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace XM4A_Packer
{
    public class XM4AProject
    {
        public string BaseDirectory { get; set; }

        public XM4AManifest Manifest { get; set; } = new XM4AManifest();

        public void Save(string fileName)
        {
            var prj = Newtonsoft.Json.JsonConvert.SerializeObject(this);
            if (File.Exists(fileName)) File.Delete(fileName);
            using (var fo = File.CreateText(fileName))
            {
                fo.Write(prj);
            }
        }

        public void Load(string fileName)
        {
            using (var fo = File.OpenText(fileName))
            {
                var json = fo.ReadToEnd();
                var unmap = Newtonsoft.Json.JsonConvert.DeserializeObject<XM4AProject>(json);
                BaseDirectory = unmap.BaseDirectory;
                Manifest = unmap.Manifest;
            }
        }

        [JsonIgnore]
        public List<string> ControllerFiles { get; } = new List<string>();
        [JsonIgnore]
        public List<string> APIFiles { get; } = new List<string>();
        public List<string> ConfigFiles { get; } = new List<string>();
        public List<string> PublicFiles { get; } = new List<string>();
        public List<string> MigrationFiles { get; } = new List<string>();
        public List<string> LibraryFiles { get; } = new List<string>();

        public void ReloadController()
        {
            if (string.IsNullOrWhiteSpace(Manifest?.app?.appid))
                return;
            var path = Path.Combine(BaseDirectory, "app", "Controllers", Manifest.app.appid.Replace('.', Path.DirectorySeparatorChar));
            if (Directory.Exists(path))
            {
                ControllerFiles.Clear();
                ControllerFiles.AddRange(Directory.EnumerateFiles(path, "*.*", SearchOption.AllDirectories));
            }
            path = Path.Combine(BaseDirectory, "app", "Controllers", "Api", Manifest.app.appid.Replace('.', Path.DirectorySeparatorChar));
            if (Directory.Exists(path))
            {
                APIFiles.Clear();
                APIFiles.AddRange(Directory.EnumerateFiles(path, "*.*", SearchOption.AllDirectories));
            }
        }

        public void CalculateChecksum()
        {
            try
            {
                var files = new List<string>();
                files.AddRange(ControllerFiles);
                files.AddRange(APIFiles);
                files.AddRange(ConfigFiles);
                files.AddRange(PublicFiles);
                files.AddRange(MigrationFiles);
                files.AddRange(LibraryFiles);

                var md5 = MD5.Create();
                byte[] buf = null;
                foreach (var file in files)
                {
                    if (Directory.Exists(file))
                    {
                        var df = Directory.EnumerateFiles(file, "*.*", SearchOption.AllDirectories);
                        foreach (var dfile in df)
                        {
                            using (var fi = File.OpenRead(dfile))
                            {
                                var res = md5.ComputeHash(fi);
                                if (buf == null)
                                    buf = res;
                                else
                                {
                                    for (var i = 0; i < buf.Length; i++)
                                    {
                                        buf[i] = (byte)(buf[i] ^ res[i]);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        using (var fi = File.OpenRead(file))
                        {
                            var res = md5.ComputeHash(fi);
                            if (buf == null)
                                buf = res;
                            else
                            {
                                for (var i = 0; i < buf.Length; i++)
                                {
                                    buf[i] = (byte)(buf[i] ^ res[i]);
                                }
                            }
                        }
                    }
                }
                Manifest.app.checksum = BitConverter.ToString(buf).Replace("-", "").ToLower();
            }
            catch
            {
                Manifest.app.checksum = null;
            }
        }

        public void Build(string fileName)
        {
            ReloadController();
            CalculateChecksum();
            if (Manifest.app.checksum == null)
                throw new Exception("Checksum calculation failed");
            var files = new Dictionary<string, string>();
            var tempdir = "temp";
            if (Directory.Exists(tempdir))
                Directory.Delete(tempdir, true);
            Directory.CreateDirectory(tempdir);

            foreach (var c in ControllerFiles)
            {
                var v = GetFiles(c, Path.Combine(tempdir, "controller"));
                foreach (var kv in v)
                    files[kv.Key] = kv.Value;
            }
            foreach (var c in APIFiles)
            {
                var v = GetFiles(c, Path.Combine(tempdir, "api"));
                foreach (var kv in v)
                    files[kv.Key] = kv.Value;
            }
            foreach (var c in ConfigFiles)
            {
                var v = GetFiles(c, Path.Combine(tempdir, "config"));
                foreach (var kv in v)
                    files[kv.Key] = kv.Value;
            }
            foreach (var c in PublicFiles)
            {
                var v = GetFiles(c, Path.Combine(tempdir, "public"));
                foreach (var kv in v)
                    files[kv.Key] = kv.Value;
            }
            foreach (var c in MigrationFiles)
            {
                var v = GetFiles(c, Path.Combine(tempdir, "migration"));
                foreach (var kv in v)
                    files[kv.Key] = kv.Value;
            }
            foreach (var c in LibraryFiles)
            {
                var v = GetFiles(c, Path.Combine(tempdir, "lib"));
                foreach (var kv in v)
                    files[kv.Key] = kv.Value;
            }

            try
            {
                foreach (var kv in files)
                {
                    var dest = Path.GetFullPath(kv.Value);
                    var destdir = Path.GetDirectoryName(dest);
                    if (!Directory.Exists(destdir))
                        Directory.CreateDirectory(destdir);
                    File.Copy(kv.Key, dest);
                }
                var mig = Newtonsoft.Json.JsonConvert.SerializeObject(Manifest);
                using (var fo = File.CreateText(Path.Combine(tempdir, "manifest.json")))
                {
                    fo.Write(mig);
                }

                // ready to pack
                ZipFile.CreateFromDirectory(Path.GetFullPath(tempdir), fileName);
            }
            finally
            {
                if (Directory.Exists(tempdir))
                    Directory.Delete(tempdir, true);
            }
        }

        private Dictionary<string, string> GetFiles(string path, string dir)
        {
            var d = new Dictionary<string, string>();
            if (Directory.Exists(path))
            {
                var fls = Directory.EnumerateFiles(path, "*.*", SearchOption.AllDirectories);
                foreach (var f in fls)
                {
                    var subs = f.Substring(path.Length);
                    if (subs.StartsWith("\\") || subs.StartsWith("/"))
                        subs = subs.Substring(1);
                    d.Add(f, Path.Combine(dir, Path.GetFileName(path), subs));
                }
            }
            else
            {
                d.Add(path, Path.Combine(dir, Path.GetFileName(path)));
            }
            return d;
        }
    }

    public class XM4AManifest
    {
        public XM4AManifestApp app { get; set; } = new XM4AManifestApp();

        public List<XM4AManifestPrerequisite> prerequisites { get; } = new List<XM4AManifestPrerequisite>();

        public XM4AManifestUninstall uninstall { get; set; } = new XM4AManifestUninstall();

        public List<string> licenseServer { get; } = new List<string>();
    }

    public class XM4AManifestApp
    {
        public string name { get; set; }

        public string developer { get; set; }

        public string publisher { get; set; }

        public string appid { get; set; }

        public string version { get; set; }

        public string checksum { get; set; }
    }

    public class XM4AManifestPrerequisite
    {
        public string appid { get; set; }
        public string version { get; set; }
    }

    public class XM4AManifestUninstall
    {
        public bool regress { get; set; }
    }
}
