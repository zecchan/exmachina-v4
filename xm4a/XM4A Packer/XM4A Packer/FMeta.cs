﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XM4A_Packer
{
    public partial class FMeta : Form
    {
        public FMeta()
        {
            InitializeComponent();
            if (FMain.Project != null)
            {
                txAppId.Text = FMain.Project.Manifest.app.appid;
                txAppName.Text = FMain.Project.Manifest.app.name;
                txDev.Text = FMain.Project.Manifest.app.developer;
                txPub.Text = FMain.Project.Manifest.app.publisher;
                txVersion.Text = FMain.Project.Manifest.app.version;
            }
            dgv.Rows.Clear();
            foreach (var pr in FMain.Project.Manifest.prerequisites)
            {
                var rw = new DataGridViewRow();
                rw.Cells.Add(new DataGridViewTextBoxCell()
                {
                    Value = pr.appid
                });
                rw.Cells.Add(new DataGridViewTextBoxCell()
                {
                    Value = pr.version
                });
                dgv.Rows.Add(rw);
            }
            HasChanges = false;
        }

        private bool HasChanges { get; set; }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void valueChanges(object sender, EventArgs e)
        {
            HasChanges = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            HasChanges = false;

            try
            {
                Version.Parse(txVersion.Text);
            }
            catch {
                MessageBox.Show("Version is not valid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            FMain.Project.Manifest.app.appid = txAppId.Text;
            FMain.Project.Manifest.app.name = txAppName.Text;
            FMain.Project.Manifest.app.developer = txDev.Text;
            FMain.Project.Manifest.app.publisher = txPub.Text;
            FMain.Project.Manifest.app.version = txVersion.Text;
            FMain.HasChanges = true;

            FMain.Project.Manifest.prerequisites.Clear();
            foreach (var r in dgv.Rows)
            {
                if (r is DataGridViewRow rw)
                {
                    var appid = rw.Cells[0].Value?.ToString();
                    var ver = rw.Cells[1].Value?.ToString();
                    try
                    {
                        if (!string.IsNullOrWhiteSpace(appid))
                        {
                            var v = Version.Parse(ver);
                            var pr = new XM4AManifestPrerequisite()
                            {
                                appid = appid,
                                version = v.ToString()
                            };
                            FMain.Project.Manifest.prerequisites.Add(pr);
                        }
                    }
                    catch { }
                }
            }
            FMain.Project.Manifest.uninstall.regress = cxRegress.Checked;

            Close();
        }

        private void FMeta_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (HasChanges && MessageBox.Show("There are some changes made, are you sure to exit?\r\nUnsaved changes will be lost.", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
            {
                e.Cancel = true;
                return;
            }
        }

        private void dgv_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            HasChanges = true;
        }
    }
}
