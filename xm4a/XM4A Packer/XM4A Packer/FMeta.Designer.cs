﻿
namespace XM4A_Packer
{
    partial class FMeta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txAppName = new System.Windows.Forms.TextBox();
            this.txAppId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txDev = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txPub = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txVersion = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.cxRegress = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Application Name";
            // 
            // txAppName
            // 
            this.txAppName.Location = new System.Drawing.Point(146, 29);
            this.txAppName.MaxLength = 64;
            this.txAppName.Name = "txAppName";
            this.txAppName.Size = new System.Drawing.Size(243, 20);
            this.txAppName.TabIndex = 1;
            this.txAppName.TextChanged += new System.EventHandler(this.valueChanges);
            // 
            // txAppId
            // 
            this.txAppId.Location = new System.Drawing.Point(146, 55);
            this.txAppId.MaxLength = 64;
            this.txAppId.Name = "txAppId";
            this.txAppId.Size = new System.Drawing.Size(243, 20);
            this.txAppId.TabIndex = 3;
            this.txAppId.TextChanged += new System.EventHandler(this.valueChanges);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Application ID";
            // 
            // txDev
            // 
            this.txDev.Location = new System.Drawing.Point(146, 81);
            this.txDev.MaxLength = 64;
            this.txDev.Name = "txDev";
            this.txDev.Size = new System.Drawing.Size(243, 20);
            this.txDev.TabIndex = 5;
            this.txDev.TextChanged += new System.EventHandler(this.valueChanges);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Developer";
            // 
            // txPub
            // 
            this.txPub.Location = new System.Drawing.Point(146, 107);
            this.txPub.MaxLength = 64;
            this.txPub.Name = "txPub";
            this.txPub.Size = new System.Drawing.Size(243, 20);
            this.txPub.TabIndex = 7;
            this.txPub.TextChanged += new System.EventHandler(this.valueChanges);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Publisher";
            // 
            // txVersion
            // 
            this.txVersion.Location = new System.Drawing.Point(146, 133);
            this.txVersion.MaxLength = 64;
            this.txVersion.Name = "txVersion";
            this.txVersion.Size = new System.Drawing.Size(243, 20);
            this.txVersion.TabIndex = 9;
            this.txVersion.TextChanged += new System.EventHandler(this.valueChanges);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Version";
            // 
            // dgv
            // 
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            this.dgv.Location = new System.Drawing.Point(25, 179);
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersWidth = 25;
            this.dgv.Size = new System.Drawing.Size(364, 213);
            this.dgv.TabIndex = 10;
            this.dgv.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellValueChanged);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Application ID";
            this.Column1.Name = "Column1";
            this.Column1.Width = 98;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Version";
            this.Column2.Name = "Column2";
            this.Column2.Width = 67;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 161);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Prerequisites";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(209, 409);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 29);
            this.button1.TabIndex = 12;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(302, 409);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(87, 29);
            this.button2.TabIndex = 13;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cxRegress
            // 
            this.cxRegress.AutoSize = true;
            this.cxRegress.Location = new System.Drawing.Point(25, 416);
            this.cxRegress.Name = "cxRegress";
            this.cxRegress.Size = new System.Drawing.Size(172, 17);
            this.cxRegress.TabIndex = 14;
            this.cxRegress.Text = "Regress Database on Uninstall";
            this.cxRegress.UseVisualStyleBackColor = true;
            this.cxRegress.CheckedChanged += new System.EventHandler(this.valueChanges);
            // 
            // FMeta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(424, 450);
            this.Controls.Add(this.cxRegress);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.txVersion);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txPub);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txDev);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txAppId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txAppName);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FMeta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manifest Metadata";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FMeta_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txAppName;
        private System.Windows.Forms.TextBox txAppId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txDev;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txPub;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txVersion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox cxRegress;
    }
}