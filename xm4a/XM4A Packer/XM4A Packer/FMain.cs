﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XM4A_Packer
{
    public partial class FMain : Form
    {
        public static XM4AProject Project { get; set; }
        public string ProjectFilePath { get; set; }

        private List<string> Recent { get; } = new List<string>();

        public static bool HasChanges { get; set; }
        public FMain()
        {
            InitializeComponent();

            cbGroup.SelectedIndex = 0;

            try
            {
                if (File.Exists("recent"))
                    using (var fi = File.OpenText("recent"))
                    {
                        var line = fi.ReadLine();
                        while (line != null)
                        {
                            Recent.Add(line);
                            line = fi.ReadLine();
                        }
                    }
            }
            catch { }

            var dgc = new DataGridViewTextBoxColumn()
            {
                HeaderText = "File Path",
                SortMode = DataGridViewColumnSortMode.NotSortable
            };
            dgv.Columns.Add(dgc);

            dgc = new DataGridViewTextBoxColumn()
            {
                HeaderText = "Source",
                SortMode = DataGridViewColumnSortMode.NotSortable
            };
            dgv.Columns.Add(dgc);

            RefreshUI();
        }

        public void AddRecent(string filePath)
        {
            if (!Recent.Any(x => x.ToLower() == filePath.ToLower()))
            {
                Recent.Insert(0, filePath);
            }
            else
            {
                var nr = Recent.Where(x => x.ToLower() != filePath.ToLower()).ToArray();
                Recent.Clear();
                Recent.Add(filePath);
                Recent.AddRange(nr);
            }
            try
            {
                if (File.Exists("recent")) File.Delete("recent");
                using (var fi = File.CreateText("recent"))
                {
                    foreach (var r in Recent)
                        fi.WriteLine(r);
                }
            }
            catch { }
        }

        public void ReloadData()
        {
            ReloadDGV();
        }

        public void ReloadDGV()
        {
            dgv.Rows.Clear();
            if (Project != null)
            {
                Project.ReloadController();

                foreach (var f in Project.ControllerFiles)
                {
                    var rw = new DataGridViewRow();
                    rw.Cells.Add(new DataGridViewTextBoxCell()
                    {
                        Value = f
                    });
                    rw.Cells.Add(new DataGridViewTextBoxCell()
                    {
                        Value = "controller"
                    });
                    rw.DefaultCellStyle.ForeColor = Color.Blue;
                    rw.Visible = cbGroup.SelectedIndex == 0 || cbGroup.SelectedIndex == 1;
                    dgv.Rows.Add(rw);
                }
                foreach (var f in Project.APIFiles)
                {
                    var rw = new DataGridViewRow();
                    rw.Cells.Add(new DataGridViewTextBoxCell()
                    {
                        Value = f
                    });
                    rw.Cells.Add(new DataGridViewTextBoxCell()
                    {
                        Value = "api"
                    });
                    rw.DefaultCellStyle.ForeColor = Color.Red;
                    rw.Visible = cbGroup.SelectedIndex == 0 || cbGroup.SelectedIndex == 2;
                    dgv.Rows.Add(rw);
                }
                foreach (var f in Project.ConfigFiles)
                {
                    var rw = new DataGridViewRow();
                    rw.Cells.Add(new DataGridViewTextBoxCell()
                    {
                        Value = f
                    });
                    rw.Cells.Add(new DataGridViewTextBoxCell()
                    {
                        Value = "config"
                    });
                    rw.Visible = cbGroup.SelectedIndex == 0 || cbGroup.SelectedIndex == 3;
                    rw.DefaultCellStyle.ForeColor = Color.DarkGreen;
                    dgv.Rows.Add(rw);
                }
                foreach (var f in Project.PublicFiles)
                {
                    var rw = new DataGridViewRow();
                    rw.Cells.Add(new DataGridViewTextBoxCell()
                    {
                        Value = f
                    });
                    rw.Cells.Add(new DataGridViewTextBoxCell()
                    {
                        Value = "public"
                    });
                    rw.Visible = cbGroup.SelectedIndex == 0 || cbGroup.SelectedIndex == 4;
                    dgv.Rows.Add(rw);
                }
                foreach (var f in Project.MigrationFiles)
                {
                    var rw = new DataGridViewRow();
                    rw.Cells.Add(new DataGridViewTextBoxCell()
                    {
                        Value = f
                    });
                    rw.Cells.Add(new DataGridViewTextBoxCell()
                    {
                        Value = "migration"
                    });
                    rw.Visible = cbGroup.SelectedIndex == 0 || cbGroup.SelectedIndex == 5;
                    rw.DefaultCellStyle.ForeColor = Color.DarkOrange;
                    dgv.Rows.Add(rw);
                }
                foreach (var f in Project.LibraryFiles)
                {
                    var rw = new DataGridViewRow();
                    rw.Cells.Add(new DataGridViewTextBoxCell()
                    {
                        Value = f
                    });
                    rw.Cells.Add(new DataGridViewTextBoxCell()
                    {
                        Value = "lib"
                    });
                    rw.Visible = cbGroup.SelectedIndex == 0 || cbGroup.SelectedIndex == 6;
                    rw.DefaultCellStyle.ForeColor = Color.Purple;
                    dgv.Rows.Add(rw);
                }
            }
        }

        public void RefreshUI()
        {
            miClose.Enabled =
            saveToolStripMenuItem.Enabled = saveAsToolStripMenuItem.Enabled =
                miProject.Enabled = pnlControl.Enabled =
                dgv.Enabled = Project != null;
            if (Project != null)
            {
                Text = "XM4A Packer - " + ProjectFilePath + (HasChanges ? "*" : "");
            }
            else Text = "XM4A Packer";
            miRecent.DropDownItems.Clear();
            foreach (var r in Recent)
            {
                var spl = r.Split(new char[] { '/', '\\' }, StringSplitOptions.RemoveEmptyEntries);
                string label = "";
                if (spl.Length > 5)
                {
                    var na = new string[4];
                    label = spl[0] + Path.DirectorySeparatorChar + "...";
                    for (var i = spl.Length - 4; i < spl.Length; i++)
                    {
                        label += Path.DirectorySeparatorChar + spl[i];
                    }
                }
                else
                {
                    label = r;
                }
                var mi = new ToolStripMenuItem()
                {
                    Text = label,
                    Tag = r
                };
                mi.Click += miRecentDDClick;
                miRecent.DropDownItems.Add(mi);
            }
        }

        private void miRecentDDClick(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem mi)
            {
                if (mi.Tag is string path)
                {
                    OpenProject(path);
                }
            }
        }

        public bool CloseProject()
        {
            if (Project != null && HasChanges && MessageBox.Show("Are you sure that you want to close the current project?\r\nUnsaved changes will be lost.", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                return false;
            Project = null;
            cbGroup.SelectedIndex = 0;
            ReloadData();
            RefreshUI();
            return true;
        }

        public void SaveProject(bool saveAs = false)
        {
            if (Project == null) return;
            if (saveAs)
            {
                if (sfd.ShowDialog() != DialogResult.OK)
                    return;
                ProjectFilePath = sfd.FileName;
            }
            try
            {
                Project.Save(ProjectFilePath);
                HasChanges = false;
                RefreshUI();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to save project: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void OpenProject(string path = null)
        {
            if (!CloseProject()) return;
            if (path == null)
            {
                if (ofd.ShowDialog() != DialogResult.OK)
                    return;
                path = ofd.FileName;
            }

            try
            {
                ProjectFilePath = path;
                Project = new XM4AProject();
                Project.Load(ProjectFilePath);
                if (!Directory.Exists(Project.BaseDirectory))
                {
                    MessageBox.Show("Failed to open XM4 root path, directory does not exists", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (!Directory.Exists(Path.Combine(Project.BaseDirectory, "app", "Config", "Xm4")))
                {
                    MessageBox.Show("Failed to open XM4 root path, cannot find app/Config/Xm4 directory inside", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                AddRecent(ProjectFilePath);
                ReloadData();
                RefreshUI();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to open project: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void NewProject()
        {
            if (!CloseProject()) return;
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    ProjectFilePath = sfd.FileName;
                    Project = new XM4AProject();
                    CommonOpenFileDialog ofd = new CommonOpenFileDialog()
                    {
                        IsFolderPicker = true,
                        Title = "Please select XM4 root path"
                    };
                    if (ofd.ShowDialog() == CommonFileDialogResult.Ok)
                    {
                        Project.BaseDirectory = ofd.FileName;
                        if (!Directory.Exists(Project.BaseDirectory))
                        {
                            MessageBox.Show("Failed to open XM4 root path, directory does not exists", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        if (!Directory.Exists(Path.Combine(Project.BaseDirectory, "app", "Config", "Xm4")))
                        {
                            MessageBox.Show("Failed to open XM4 root path, cannot find app/Config/Xm4 directory inside", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        Project.Save(ProjectFilePath);
                        AddRecent(ProjectFilePath);
                        ReloadData();
                        RefreshUI();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to create new project: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void miNew_Click(object sender, EventArgs e)
        {
            NewProject();
        }

        private void miOpen_Click(object sender, EventArgs e)
        {
            OpenProject();
        }

        private void miClose_Click(object sender, EventArgs e)
        {
            CloseProject();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveProject();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveProject(true);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            if (Project == null) return;
            if (string.IsNullOrWhiteSpace(Project?.Manifest?.app?.appid))
            {
                MessageBox.Show("It seems that you haven't configure the project's meta yet, please configure first!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            ReloadData();
        }

        private void cbGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgv.AllowDrop = btnAdd.Enabled = btnRemove.Enabled = cbGroup.SelectedIndex > 2;
            if (Project == null) return;
            if (string.IsNullOrWhiteSpace(Project?.Manifest?.app?.appid))
            {
                MessageBox.Show("It seems that you haven't configure the project's meta yet, please configure first!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            var seeAll = cbGroup.SelectedIndex == 0;
            foreach (var r in dgv.Rows)
            {
                if (r is DataGridViewRow rw)
                {
                    rw.Visible = seeAll || rw.Cells[1].Value?.ToString() == cbGroup.Text;
                }
            }
        }

        private void miMeta_Click(object sender, EventArgs e)
        {
            using (var f = new FMeta())
            {
                f.ShowDialog(this);
                ReloadData();
                RefreshUI();
            }
        }

        private void manifestjsonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var f = new FManifest())
            {
                f.ShowDialog(this);
            }
        }

        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            btnRemove.Enabled = false;
            if (Project == null) return;
            var canRemove = true;
            foreach (var r in dgv.SelectedRows)
            {
                if (r is DataGridViewRow rw)
                {
                    if (rw.Cells[1].Value?.ToString() == "controller" || rw.Cells[1].Value?.ToString() == "api")
                    {
                        canRemove = false;
                        break;
                    }
                }
            }
            btnRemove.Enabled = canRemove;
        }

        private void dgv_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.None;
            if (Project == null || cbGroup.SelectedIndex <= 2) return;
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }

        private void dgv_DragDrop(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.None;
            if (Project == null || cbGroup.SelectedIndex <= 2) return;
            try
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                var ix = cbGroup.SelectedIndex;
                if (ix == 3) Project.ConfigFiles.AddRange(files);
                if (ix == 4) Project.PublicFiles.AddRange(files);
                if (ix == 5) Project.MigrationFiles.AddRange(files);
                if (ix == 6) Project.LibraryFiles.AddRange(files);

                HasChanges = true;
                ReloadData();
                RefreshUI();
            }
            catch { }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure to delete these files?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                return;
            List<DataGridViewRow> rows = new List<DataGridViewRow>();
            try
            {
                foreach (var r in dgv.SelectedRows)
                {
                    if (r is DataGridViewRow dr) rows.Add(dr);
                }
                foreach (var r in rows)
                {
                    var val = r.Cells[0].Value?.ToString();
                    var pos = r.Cells[1].Value?.ToString();
                    if (pos == "config") Project.ConfigFiles.Remove(val);
                    if (pos == "public") Project.PublicFiles.Remove(val);
                    if (pos == "migration") Project.MigrationFiles.Remove(val);
                    if (pos == "lib") Project.LibraryFiles.Remove(val);
                    dgv.Rows.Remove(r);
                    HasChanges = true;
                }
                RefreshUI();
            }
            catch { }
        }

        private void FMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!CloseProject())
            {
                e.Cancel = true;
            }
        }

        private void buildToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sfdBuild.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (File.Exists(sfdBuild.FileName))
                        File.Delete(sfdBuild.FileName);
                    Project.Build(sfdBuild.FileName);
                    MessageBox.Show("Build success!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to build project: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
