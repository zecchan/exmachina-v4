﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XM4A_Packer
{
    public partial class FManifest : Form
    {
        public FManifest()
        {
            InitializeComponent();
        }

        private void FManifest_Load(object sender, EventArgs e)
        {
            if (FMain.Project != null)
            {
                try
                {
                    FMain.Project.CalculateChecksum();
                }
                catch { }
                textBox1.Text = Newtonsoft.Json.JsonConvert.SerializeObject(FMain.Project?.Manifest, Newtonsoft.Json.Formatting.Indented);
            }
        }
    }
}
